var ready;

ready = function() {
	CodeMirror.fromTextArea(document.getElementById("plugin_css"), {
		mode: "css",
		lineNumbers: true,
		lineWrapping: true
	});
	
	CodeMirror.fromTextArea(document.getElementById("plugin_javascript"), {
		mode: "javascript",
		lineNumbers: true,
		lineWrapping: true
	});

	CodeMirror.fromTextArea(document.getElementById("plugin_html"), {
		mode: "html",
		lineNumbers: true,
		lineWrapping: true
	});
}

$(document).ready(ready);
$(document).on('page:load', ready);
