class UpdateDeviceToDeviceLinkTableName < ActiveRecord::Migration
  def change
    rename_table :device_to_device_link, :device_to_device_links
  end
end
