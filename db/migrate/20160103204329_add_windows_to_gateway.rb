class AddWindowsToGateway < ActiveRecord::Migration
	def change
		rename_column :windows, :node_id, :device_id
	end
end
