class ChangeDevicesTypeToDeviceType < ActiveRecord::Migration
  def change
    remove_column :devices, :type, :string
    add_column :devices, :device_type, :string
  end
end
