class WindowsHaveTwoSizes < ActiveRecord::Migration
	def change
		remove_column :windows, :position_x, :integer
		remove_column :windows, :position_y, :integer
		remove_column :windows, :height, :integer
		remove_column :windows, :width, :integer

		add_column :windows, :count, :integer, null: false
		add_column :windows, :is_large, :boolean
	end
end
