class DeviceToDeviceLink < ActiveRecord::Migration
  def change
    create_table :device_to_device_link do |t|
      t.integer :parent
      t.integer :child

      t.timestamps null: false
    end
  end
end
