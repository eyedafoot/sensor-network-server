class CreatePrivileges < ActiveRecord::Migration
  def change
    create_table :privileges do |t|
      t.string :name
      t.integer :privilege_level

      t.timestamps null: false
    end
  end
end
