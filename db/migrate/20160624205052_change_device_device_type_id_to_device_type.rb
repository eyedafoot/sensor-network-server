class ChangeDeviceDeviceTypeIdToDeviceType < ActiveRecord::Migration
  def change
    remove_column :devices, :device_type_id, :integer
    add_column :devices, :type, :string
  end
end
