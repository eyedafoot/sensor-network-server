class ChangeFieldNameToString < ActiveRecord::Migration
	def change
		remove_column :fields, :name, :integer
		add_column :fields, :name, :string
	end
end
