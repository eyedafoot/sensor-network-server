class DeleteTimeBasedAndEnabledFromField < ActiveRecord::Migration
  def change
    remove_column :fields, :is_time_dependent, :tinyint
    remove_column :fields, :is_enabled, :tinyint
  end
end
