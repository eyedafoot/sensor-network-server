class CreateGateways < ActiveRecord::Migration
  def change
    create_table :gateways do |t|
      t.string :mac_address

      t.timestamps null: false
    end
  end
end
