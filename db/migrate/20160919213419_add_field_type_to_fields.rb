class AddFieldTypeToFields < ActiveRecord::Migration
  def change
    add_column :fields, :field_type, :string
  end
end
