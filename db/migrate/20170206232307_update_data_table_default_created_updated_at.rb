class UpdateDataTableDefaultCreatedUpdatedAt < ActiveRecord::Migration
  CREATE_TIMESTAMP = 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP'
  def change
    change_column :data, :created_at, CREATE_TIMESTAMP
    change_column :data, :updated_at, CREATE_TIMESTAMP
  end
end
