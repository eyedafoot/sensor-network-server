class CreateSettingsTable < ActiveRecord::Migration
  def change
    create_table :settings do |t|
      t.string :item_code
      t.string :item_description

      t.timestamps null: false
    end
  end
end
