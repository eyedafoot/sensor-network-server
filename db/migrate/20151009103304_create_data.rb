class CreateData < ActiveRecord::Migration
  def change
    create_table :data do |t|
      t.integer :field_id
      t.float :value
      t.datetime :created_at

      t.timestamps null: false
    end
  end
end
