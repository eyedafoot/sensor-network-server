class CreateFields < ActiveRecord::Migration
  def change
    create_table :fields do |t|
      t.integer :name
      t.integer :node_id
      t.boolean :is_time_dependent
      t.boolean :is_enabled

      t.timestamps null: false
    end
  end
end
