class UpdateToDecoupleNodes < ActiveRecord::Migration
  def change

  	add_column :users, :master_key, :string

	create_table :devices do |t|
		t.string :name
		t.string :key
		t.integer :user_id
	end

	remove_column :nodes, :gateway_id, :integer

	add_column :plugins, :user_id, :integer
	rename_column :plugins, :is_private, :is_protected

	create_table :commands do |t|
		t.string :name
		t.string :value
		t.integer :gateway_id
		t.integer :command_id
	end

  end
end
