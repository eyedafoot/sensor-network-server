class ChangeDevicesTypeIdToDeviceTypeId < ActiveRecord::Migration
  def change
    remove_column :devices, :type_id, :string
    add_column :devices, :device_type_id, :integer
  end
end
