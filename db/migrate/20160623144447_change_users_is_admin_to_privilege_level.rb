class ChangeUsersIsAdminToPrivilegeLevel < ActiveRecord::Migration
  def change
    remove_column :users, :is_admin, :string
    add_column :users, :privilege_level, :integer
  end
end
