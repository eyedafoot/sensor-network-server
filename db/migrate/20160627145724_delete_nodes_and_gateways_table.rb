class DeleteNodesAndGatewaysTable < ActiveRecord::Migration
  def change
    drop_table :nodes
    drop_table :gateways
  end
end
