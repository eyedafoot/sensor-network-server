class ChangeCommandGatewayIdToDeviceId < ActiveRecord::Migration
  def change
    remove_column :commands, :gateway_id, :integer
    add_column :commands, :device_id, :integer
  end
end
