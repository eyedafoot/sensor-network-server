class ChangeDevicesTypeToInteger < ActiveRecord::Migration
  def change
    remove_column :devices, :type, :string
    add_column :devices, :type_id, :integer
  end
end
