class CreateDeviceApprovalQueue < ActiveRecord::Migration
  def change
    create_table :device_approval_queues do |t|
      t.integer :address, unique: true
      t.string :device_id

      t.timestamps null: false
    end
  end
end
