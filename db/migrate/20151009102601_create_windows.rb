class CreateWindows < ActiveRecord::Migration
  def change
    create_table :windows do |t|
      t.integer :node_id
      t.integer :plugin_id
      t.integer :width
      t.integer :height
      t.integer :position_x
      t.integer :position_y

      t.timestamps null: false
    end
  end
end
