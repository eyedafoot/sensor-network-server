class ChangeDeviceApprovalQueuesAddressFromIntToString < ActiveRecord::Migration
  def change
    remove_column :device_approval_queues, :address, :integer
    add_column :device_approval_queues, :address, :string
  end
end
