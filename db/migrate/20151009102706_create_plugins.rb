class CreatePlugins < ActiveRecord::Migration
  def change
    create_table :plugins do |t|
      t.string :name
      t.text :html
      t.text :css
      t.text :javascript

      t.timestamps null: false
    end
  end
end
