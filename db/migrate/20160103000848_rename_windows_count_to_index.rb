class RenameWindowsCountToIndex < ActiveRecord::Migration
	def change
		rename_column :windows, :count, :index
	end
end
