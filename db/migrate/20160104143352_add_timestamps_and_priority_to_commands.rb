class AddTimestampsAndPriorityToCommands < ActiveRecord::Migration
	def change
		add_column :commands, :created_at, :datetime
		add_column :commands, :updated_at, :datetime
		add_column :commands, :priority, :integer, null: false
	end
end
