class AddIsPrivateColumnToPlugins < ActiveRecord::Migration
  def change
  	add_column :plugins, :is_private, :boolean
  end
end
