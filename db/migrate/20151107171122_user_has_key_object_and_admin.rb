class UserHasKeyObjectAndAdmin < ActiveRecord::Migration
	def change
		remove_column :users, :master_key, :string
		add_column :users, :key_id, :integer
		add_column :users, :is_admin, :boolean
	end
end
