class StringKeysToObjectKeys < ActiveRecord::Migration
  def change
		remove_column :devices, :key, :string
		add_column :devices, :key_id, :integer
		remove_column :nodes, :key, :string
		add_column :nodes, :key_id, :integer
		remove_column :gateways, :key, :string
		add_column :gateways, :key_id, :integer
  end
end
