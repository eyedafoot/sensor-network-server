class CreateSettingsAndVariables < ActiveRecord::Migration
	def change

		create_table :settings do |t|
			t.string :name, null: false
			t.integer :plugin_id
		end

		create_table :variables do |t|
			t.string :value
			t.integer :window_id
			t.integer :setting_id
		end
		
	end
end
