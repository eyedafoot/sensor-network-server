class AddActiveToCommands < ActiveRecord::Migration
  def change
    add_column :commands, :active, :boolean, default: false
  end
end
