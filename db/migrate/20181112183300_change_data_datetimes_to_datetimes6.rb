class ChangeDataDatetimesToDatetimes6 < ActiveRecord::Migration
  def change
    execute 'ALTER TABLE `data` CHANGE `created_at` `created_at` datetime(6) DEFAULT CURRENT_TIMESTAMP(6) NOT NULL'
    execute 'ALTER TABLE `data` CHANGE `updated_at` `updated_at` datetime(6) DEFAULT CURRENT_TIMESTAMP(6) NOT NULL'
  end
end
