class DeleteDeviceType < ActiveRecord::Migration
  def change
    drop_table :device_types
  end
end
