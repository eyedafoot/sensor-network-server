class AddAddressFieldToDevices < ActiveRecord::Migration
  def change
    add_column :devices, :address, :string
  end
end
