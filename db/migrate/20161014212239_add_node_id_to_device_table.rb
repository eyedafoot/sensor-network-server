class AddNodeIdToDeviceTable < ActiveRecord::Migration
  def change
    add_column :devices, :node_id, :integer
  end
end
