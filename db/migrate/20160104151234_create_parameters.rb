class CreateParameters < ActiveRecord::Migration
  def change
    create_table :parameters do |t|
      t.string :name
      t.string :value
      t.integer :command_id, null: false
    end

    remove_column :commands, :value, :string
    remove_column :commands, :command_id, :integer
  end
end
