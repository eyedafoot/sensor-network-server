class UseBcrypt < ActiveRecord::Migration
  def change
  	rename_column :users, :hashed_password, :password_digest
  	remove_column :users, :salt
  end
end
