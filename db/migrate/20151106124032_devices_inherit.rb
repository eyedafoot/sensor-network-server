class DevicesInherit < ActiveRecord::Migration
	def change
		add_column :devices, :type, :string
		remove_column :devices, :user_id, :integer
		remove_column :nodes, :name, :string
		remove_column :nodes, :key_id, :integer
		remove_column :gateways, :key_id, :integer
	end
end
