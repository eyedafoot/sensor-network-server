class FixKeysRelationship < ActiveRecord::Migration
	def change
		remove_column :devices, :key_id, :integer
		remove_column :users, :key_id, :integer

		add_column :keys, :user_id, :integer
		add_column :keys, :device_id, :integer
	end
end
