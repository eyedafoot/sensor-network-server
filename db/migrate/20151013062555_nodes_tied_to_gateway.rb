class NodesTiedToGateway < ActiveRecord::Migration
  def change
  	rename_column :gateways, :mac_address, :key
  	add_column :nodes, :gateway_id, :integer
  	add_column :nodes, :key, :string
  end
end
