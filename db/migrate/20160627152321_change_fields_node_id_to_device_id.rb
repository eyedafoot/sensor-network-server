class ChangeFieldsNodeIdToDeviceId < ActiveRecord::Migration
  def change
    remove_column :fields, :node_id, :integer
    add_column :fields, :device_id, :integer
  end
end
