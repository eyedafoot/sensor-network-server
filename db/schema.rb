# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20181112183300) do

  create_table "access_rights", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.integer  "device_id",  limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "articles", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.text     "content",    limit: 65535
    t.integer  "topic_id",   limit: 4
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "commands", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "priority",   limit: 4,                   null: false
    t.integer  "device_id",  limit: 4
    t.boolean  "active",                 default: false
  end

  create_table "data", force: :cascade do |t|
    t.integer  "field_id",   limit: 4
    t.float    "value",      limit: 24
    t.datetime "created_at",            precision: 6, null: false
    t.datetime "updated_at",            precision: 6, null: false
  end

  create_table "device_approval_queues", force: :cascade do |t|
    t.string   "device_id",  limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.string   "address",    limit: 255
  end

  create_table "device_to_device_links", force: :cascade do |t|
    t.integer  "parent",     limit: 4
    t.integer  "child",      limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "devices", force: :cascade do |t|
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.string   "name",        limit: 255
    t.string   "device_type", limit: 255
    t.string   "address",     limit: 255
    t.integer  "node_id",     limit: 4
  end

  create_table "fields", force: :cascade do |t|
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.string   "name",       limit: 255
    t.integer  "device_id",  limit: 4
    t.string   "field_type", limit: 255
  end

  create_table "keys", force: :cascade do |t|
    t.string   "value",      limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "user_id",    limit: 4
    t.integer  "device_id",  limit: 4
  end

  create_table "parameters", force: :cascade do |t|
    t.string  "name",       limit: 255
    t.string  "value",      limit: 255
    t.integer "command_id", limit: 4,   null: false
  end

  create_table "plugins", force: :cascade do |t|
    t.string   "name",         limit: 255
    t.text     "html",         limit: 65535
    t.text     "css",          limit: 65535
    t.text     "javascript",   limit: 65535
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.boolean  "is_protected"
    t.integer  "user_id",      limit: 4
    t.string   "type",         limit: 255
  end

  create_table "privileges", force: :cascade do |t|
    t.string   "name",            limit: 255
    t.integer  "privilege_level", limit: 4
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "settings", force: :cascade do |t|
    t.string   "item_code",        limit: 255
    t.string   "item_description", limit: 255
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  create_table "topics", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",           limit: 255
    t.string   "username",        limit: 255
    t.string   "password_digest", limit: 255
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.integer  "privilege_level", limit: 4
  end

  create_table "variables", force: :cascade do |t|
    t.string  "value",      limit: 255
    t.integer "window_id",  limit: 4
    t.integer "setting_id", limit: 4
  end

  create_table "windows", force: :cascade do |t|
    t.integer  "device_id",  limit: 4
    t.integer  "plugin_id",  limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.integer  "index",      limit: 4, null: false
    t.boolean  "is_large"
  end

end
