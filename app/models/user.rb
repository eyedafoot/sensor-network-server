# == Schema Information
#
# Table name: users
#
#  id              :integer          not null, primary key
#  email           :string(255)
#  username        :string(255)
#  password_digest :string(255)
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  is_admin        :boolean
#

class User < ActiveRecord::Base
	has_secure_password
	has_one :privilege
	has_one :key
	has_many :plugins
	has_many :access_rights, :dependent => :destroy 
	has_many :devices, through: :access_rights
	has_many :windows, source: :windows, through: :devices
	has_many :fields, source: :fields, through: :devices

	validates_uniqueness_of :email
	validates_uniqueness_of :username, on: :create
	validates :username, length: { minimum: 5 }, on: :create
	validates :password, length: { minimum: 5 }, on: :create
end
