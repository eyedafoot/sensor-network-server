# == Schema Information
#
# Table name: keys
#
#  id         :integer          not null, primary key
#  value      :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :integer
#  device_id  :integer
#

require 'securerandom'

class Key < ActiveRecord::Base
	def self.generate_key
		value = SecureRandom.hex(8)
		if Key.find_by_value value
			Key.generate_key
		else
			Key.new value: value
		end
	end
end
