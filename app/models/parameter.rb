# == Schema Information
#
# Table name: parameters
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  value      :string(255)
#  command_id :integer          not null
#

class Parameter < ActiveRecord::Base
	belongs_to :command
end
