# == Schema Information
#
# Table name: fields
#
#  id                :integer          not null, primary key
#  device_id         :integer
#  is_time_dependent :boolean
#  is_enabled        :boolean
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  name              :string(255)
#

class Field < ActiveRecord::Base
	belongs_to :device
	has_many :data, :dependent => :destroy

	validates :device_id, presence: true
end
