# == Schema Information
#
# Table name: data
#
#  id         :integer          not null, primary key
#  field_id   :integer
#  value      :float(24)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'csv'
require 'axlsx'

class Datum < ActiveRecord::Base
	belongs_to :field

	validates :field_id, presence: true
	validates :value, presence: true

	def self.to_csv(options, column_names)
		CSV.generate(options) do |csv|
			csv << column_names
			all.each do |data|
				csv << column_names.map do |col|
					if col == 'created_at'
						data.created_at.strftime('%Y-%m-%d %H:%M:%S.%N')
					else
						data.send(col)
					end
				end
			end
		end
	end

	def self.to_xlsx(options, column_names)
		Axlsx::Package.new do |package|
			package.workbook do |workbook|
				workbook.add_worksheet(:name => "Basic Worksheet") do |sheet|
					sheet.add_row column_names
					all.each do |data|
						sheet.add_row column_names.map{ |column_name| data.send(column_name)}
					end
				end
			end
			return package
		end
	end

end
