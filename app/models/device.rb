# == Schema Information
#
# Table name: devices
#
#  id          :integer          not null, primary key
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  name        :string(255)
#  device_type :integer
#

class Device < ActiveRecord::Base
	has_many :access_rights, :dependent => :destroy
	has_many :users, through: :access_rights
	has_many :windows, :dependent => :destroy
	has_one :key, :dependent => :destroy
  has_many :fields, :dependent => :destroy
  has_many :commands, :dependent => :destroy
  has_many :device_approval_queues, :dependent => :destroy

	validates :name, presence: true

  def next_command params
    coms = commands
    coms = coms.where(name: params[:name]) if params[:name]
    coms = coms.where(priority: params[:priority]) if params[:priority]
    coms.order(priority: :asc, created_at: :asc).first
  end

  def is_node
    device_type == "Node"
  end

  def is_gateway
    device_type == "Gateway"
  end
end
