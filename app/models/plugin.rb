# == Schema Information
#
# Table name: plugins
#
#  id           :integer          not null, primary key
#  name         :string(255)
#  html         :text(65535)
#  css          :text(65535)
#  javascript   :text(65535)
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  is_protected :boolean
#  user_id      :integer
#  type         :string(255)
#

class Plugin < ActiveRecord::Base
	self.inheritance_column = nil
	belongs_to :user
	has_many :windows, :dependent => :destroy
	has_many :settings, :dependent => :destroy
end
