class FormModels::CommandForm
  attr_accessor(
      :name,
      :priority,
      :parameters
  );

  def initialize
    self.parameters = [];
  end
end
