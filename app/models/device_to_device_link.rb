class DeviceToDeviceLink < ActiveRecord::Base
  belongs_to :device
  has_many :device
end
