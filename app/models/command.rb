# == Schema Information
#
# Table name: commands
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  device_id  :integer
#  created_at :datetime
#  updated_at :datetime
#  priority   :integer          not null
#

class Command < ActiveRecord::Base
	belongs_to :device
	has_many :parameters, :dependent => :destroy

	# Run the required server actions
	def execute
		# # Run if server command
		# if command.name == "server_email"
		# 	email = command.parameters.where(name: 'email').first.value
		# 	if email.nil?
		# 		command.parameters.create(name: 'Status', value: 'Invalid')
		# 	else
		# 		Mailer.fault_detected(email).deliver
		# 		command.parameters.create(name: 'Status', value: 'Sent')
		# 	end
		# end
	end
end
