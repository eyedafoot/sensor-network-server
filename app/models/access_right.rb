# == Schema Information
#
# Table name: access_rights
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  device_id  :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class AccessRight < ActiveRecord::Base
	belongs_to :user
	belongs_to :device
end
