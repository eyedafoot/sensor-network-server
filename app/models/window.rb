# == Schema Information
#
# Table name: windows
#
#  id         :integer          not null, primary key
#  device_id  :integer
#  plugin_id  :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  index      :integer          not null
#  is_large   :boolean
#

class Window < ActiveRecord::Base
	belongs_to :device
	belongs_to :plugin
	#has_many :variables, :dependent => :destroy
	validates :device_id, presence: true
	validates :plugin_id, presence: true

	def width
		if is_large
			width = 12
		else 
			width = 6
		end 
	end

	def height
		if is_large
			height = 700
		else 
			height = 350
		end 
	end

	# Each of the following functions will return the errors incountered

	def resize
		self.is_large = !self.is_large
		self.save ? nil : 'Could not save window (database)'
	end

	def move_up 
		next_window = Window.where(index: self.index + 1).where(device_id: self.device_id)[0]
		if next_window
			next_window.index = next_window.index - 1
			self.index = self.index + 1
			(self.save and next_window.save) ? nil : 'Could not save windows (database)'
		else 
			nil # no error (at end)
		end
	end

	def move_down 
		previous_window = Window.where(index: self.index - 1).where(device_id: self.device_id)[0]
		if previous_window
			previous_window.index = previous_window.index + 1
			self.index = self.index - 1
			(self.save and previous_window.save) ? nil : 'Could not save windows (database)'
		else
			nil # no error (at beginning)
		end
	end

	# To delete a window the current user must have access to the device
	def delete current_user
		device = current_user.devices.find_by_id self.device_id
		if device
			all_saved = true
			greater_windows = device.windows.where('`index` > ?', self.index)
			greater_windows.each do |win| 
				win.index = win.index - 1
				all_saved = win.save and all_saved
			end
			if all_saved 
				self.destroy ? nil : 'Could not destroy window (database)' 
			else 
				'Could not save other windows (database)'
			end
		else
			'User does not have access to device'
		end
	end
	
end
