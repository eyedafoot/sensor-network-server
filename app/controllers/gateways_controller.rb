class GatewaysController < ApplicationController
	before_filter :authorize

	def show
		@gateway = current_user.devices.find_by_id params[:id]
		if @gateway
			@plugins = Plugin.where(type: :Gateway)
				.where(is_protected: false)
				.concat(current_user.plugins.where(type: :Gateway))
				.uniq
			@windows = @gateway.windows.order(:index)
			nodes = []
			@child_count = 0
			begin
				@child_count = DeviceToDeviceLink.where(parent:@gateway.id).count(:child)
				links = DeviceToDeviceLink.where(parent:@gateway.id,child:current_user.access_rights.select(:device_id))
				links.each do |link|
					node = Device.find(link.child)
					nodes.push(node)
				end
			end
			@nodes = nodes

			commands = []
			begin
				@nodes.each do |node|
					node_commands = node.commands.order(priority: :asc, created_at: :asc)
					if node_commands
						node_commands.each do |command|
							commands.push(command)
						end
					end
				end
				gateway_commands = @gateway.commands.order(priority: :asc, created_at: :asc)
				gateway_commands.each do |command|
					commands.push(command)
				end
			end
			@commands = commands.sort_by{|c| [c.priority, c.created_at] }

			settings = Setting.where(item_code: 'commandtype')
			@command_types = []
			if !settings.nil? && !settings.empty?
				settings.map do |setting|
					@command_types << setting.item_description
				end
			end

			settings = Setting.where(item_code: 'commandparamtype')
			@command_param_types = []
			if !settings.nil? && !settings.empty?
				settings.map do |setting|
					@command_param_types << setting.item_description
				end
			end
		elsif Gateway.find_by_id params[:id]
			flash[:error] = 'User does not have access rights to the gateway'
			redirect_to network_path
		else
			flash[:error] = 'Gateway does not exist'
			redirect_to network_path
		end
	end

end
