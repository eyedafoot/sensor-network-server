class WindowsController < ApplicationController

	def show
		@window = current_user.windows.find_by_id params[:id]
	end

	def create
		device = current_user.devices.find_by_id params[:window][:device_id]
		plugin = Plugin.find_by_id params[:window][:plugin_id]
		if device.nil? 
			flash[:error] = 'Device does not exist'
		elsif plugin.nil?
			flash[:error] = 'Plugin does not exist'
		elsif device.device_type == plugin.type
			window = Window.new window_params(device)
			if !window.save
				flash[:error] = 'Window could not be created'
			end
		else
			flash[:error] = 'Device type does not match plugin type'
		end
		redirect_to device_path(device.id)
	end

	def resize 
		window = Window.find_by_id params[:id]
		if window.nil?
			render json: { error: 'Window could not be found' } 
		else 
			error = window.resize
			if error
				render json: { error: error }
			else
				render json: { height: window.height, width: window.width }
			end
		end
	end

	def forward
		window = Window.find_by_id params[:id]
		if window.nil?
			render json: { error: 'Window could not be found' } 
		else 
			error = window.move_up
			render json: { error: error }
		end
	end

	def backward
		window = Window.find_by_id params[:id]
		if window.nil?
			render json: { error: 'Window could not be found' } 
		else 
			error = window.move_down
			render json: { error: error }
		end
	end

	def destroy
		window = Window.find_by_id params[:id]
		if window.nil?
			render json: { error: 'Window could not be found' } 
		else 
			error = window.delete current_user
			render json: { error: error }
		end
	end

	private
	def window_params device
		params.require(:window).permit(:device_id, :plugin_id)
			.merge(index: (device.windows.maximum(:index) || -1) + 1)
			.merge(is_large: false)
	end
end
