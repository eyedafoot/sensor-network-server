require 'securerandom'

class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  helper_method :current_user, :is_admin, :is_system_admin


  def current_user
    master_key = Key.find_by_value(params[:master_key])
    if master_key
      @current_user ||= User.find_by_id master_key.user_id
    else
      @current_user ||= User.find_by_id session[:user_id]
    end
  end

  def authorize
    redirect_to login_url unless current_user;
  end

  def is_admin
    current_user.privilege_level == (Privilege.find_by_name 'Admin').id or current_user.privilege_level == (Privilege.find_by_name 'SystemAdmin').id
  end

  def is_system_admin
    current_user.privilege_level == (Privilege.find_by_name 'SystemAdmin').id
  end
end
