class DevicesController < ApplicationController
	before_filter :authorize
	before_filter :is_admin

	def index
		require 'ostruct'
		removeIds = ""
		gateways = Device.where(device_type: "Gateway")
		@deviceTrees = []
		gateways.each do |gateway|
			deviceTree = OpenStruct.new
			deviceTree.gateway = gateway
			nodes = []
			begin
				links = DeviceToDeviceLink.where(parent:gateway.id)
				links.each do |link|
					node = Device.find(link.child)
					nodes.push(node)
					removeIds += "#{node.id},"
				end
				deviceTree.nodes = nodes
			end
			@deviceTrees.push(deviceTree)
		end
		removeIds = removeIds.chomp(",")
		@devices = Device.where(device_type: "Node")
		if removeIds != ""
			@devices = @devices.where("id not in (#{removeIds})")
		end
		@queued_nodes = nodes_queued_for_approval
	end

	def new 
		@device = Device.new
		#TODO: Can't added device unless it request to join network (maybe)
	end

	def show
		@device = current_user.devices.find_by_id params[:id]

		if @device.device_type == "Node"
			redirect_to node_path @device.id
		elsif @device.device_type == "Gateway"
			redirect_to gateway_path @device.id
		elsif @device
			flash[:error] = 'Unknown device type'
			redirect_to network_path
		else 
			flash[:error] = 'Device not found'
			redirect_to network_path
		end 
	end

	def create
		#TODO: Can't added device unless it request to join network (maybe)
		@device = Device.new device_params
		@name = @device.name
		@type = @device.device_type
		if @device.save
			if @device.node_id.nil?
				@device.update(node_id: @device.id)
			end
			key = Key.generate_key
			key.device_id = @device.id
			if key.save
				redirect_to devices_path
			else 
				if @device.destroy
					flash[:error] = 'Could not save key.'
				else
					flash[:error] = 'Could not save key and device not destroyed. Database correction needed'
				end
				render 'new'
			end
		else
			flash[:error] = 'Could not save device.'
			render 'new'
		end
	end

	def edit
		@device = Device.find params[:id]
	end

	def update
		@device = Device.find params[:id]
		if @device.update device_params
			redirect_to devices_path
		else
			flash[:error] = 'Could not update device.'
			redirect_to edit_device_path(params[:id])
		end
	end

	def destroy
		@device = Device.find params[:id]
		if @device.destroy
			DeviceToDeviceLink.where("child = " + params[:id] + " OR parent = " + params[:id]).delete_all
			redirect_to devices_path
		else
			flash[:error] = 'Could not destroy device.'
			redirect_to devices_path
		end
	end

	def add_link_node_to_gateway
		begin
			link = DeviceToDeviceLink.find_by_child(params[:node_id])
			if link.update(parent:params[:gateway_id])
				render json: {success: "Successfully linked Node Id: #{params[:node_id]} to Gateway Id: #{params[:gateway_id]}"}
			else
				render json: {error: "Unable to link Node Id: #{params[:node_id]} to Gateway Id: #{params[:gateway_id]}"}
			end
		rescue
			link = DeviceToDeviceLink.new
			link.parent = params[:gateway_id]
			link.child = params[:node_id]
			if link.save
				render json: {success: "Successfully linked Node Id: #{params[:node_id]} to Gateway Id: #{params[:gateway_id]}"}
			else
				render json: {error: "Unable to link Node Id: #{params[:node_id]} to Gateway Id: #{params[:gateway_id]}"}
			end
		end
	end

	def remove_link_node_to_gateway
		begin
			link = DeviceToDeviceLink.find_by_child(params[:node_id])
			gateway_id = link.parent
			if link.destroy
				remove_node_cmd = Command.new(name:"remove-node",priority:1,device_id:gateway_id,active:true)
				remove_node_cmd.save
				remove_node_param = Parameter.new(name:"node",value:params[:node_id],command_id:remove_node_cmd.id)
				remove_node_param.save
				render json: {success: "Successfully removed link between Node Id: #{params[:node_id]} and Gateway Id: #{gateway_id}"}
			else
				render json: {error: "Unable to remove link between Node Id: #{params[:node_id]} and Gateway Id: #{gateway_id}"}
			end
		rescue
			render json: {error: "No links for Node Id: #{params[:node_id]}"}
		end
	end

	def accept_queued_node
		#TODO: Allow users to set device Ids (possibly restrict based on Ids of other associated nodes to gateway)
		queued_node = DeviceApprovalQueue.find_by_id(params[:id])
		device = Device.new(name: "node#{params[:id]}", device_type: "Node", address: queued_node.address)
		if device.save
			if device.node_id.nil?
				device.update(node_id: device.id)
			end
			key = Key.generate_key
			key.device_id = device.id
			if key.save
				queued_node.destroy
				redirect_to devices_path
				return
			else
				if @device.destroy
					flash[:error] = 'Could not save key.'
				else
					flash[:error] = 'Could not save key and device not destroyed. Database correction needed'
				end
				redirect_to new_device_path
				return
			end
		else
			flash[:error] = 'Could not save device.'
			redirect_to new_device_path
			return
		end
		redirect_to devices_path
	end

	def reject_queued_node
		queued_node = DeviceApprovalQueue.find_by_id(params[:id])
		queued_node.destroy

		redirect_to devices_path
	end

	def number_of_queued_nodes
		nodes = nodes_queued_for_approval
		if nodes.nil?
			render json: 0
		else
			render json: nodes.length
		end
	end

	private

	def device_params
		params.require(:device).permit(:name, :device_type, :address, :node_id)
	end

	def nodes_queued_for_approval
		user_gateways = current_user.devices.where(device_type: 'Gateway');
		if user_gateways
			return DeviceApprovalQueue.all.where(device_id: user_gateways.ids)
		end
		return nil;
	end
end
