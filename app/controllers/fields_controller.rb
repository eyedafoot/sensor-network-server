class FieldsController < ApplicationController
	before_filter :authorize
	before_filter :default_field_type

	def create
		@field = Field.new default_params
		if @field.save
			#render json: {success: "Successfully created field: #{@field.id}"}
			#flash[:success] = "Successfully created field: #{@field.id}"

			settings = Setting.where(item_code: 'nodetype')
			@field_types = []
			if !settings.nil? && !settings.empty?
				settings.map do |setting|
					@field_types << setting.item_description
				end
			end

			render 'new', layout: false
			#render json: {success: "Created new field"}
		else
			render json: {error: "Could not create field"}
			#flash[:error] = 'Could not create field'
		end
	end

	def destroy
		@field = current_user.fields.find_by_id params[:field][:id]
		if @field.destroy
			render json: {success: "Successfully deleted field: #{params[:field][:id]}"}
			#render nothing: true
		else
			render json: {error: "Could not destroy field: #{params[:field][:id]}"}
			#flash[:error] = 'Could not destroy field'
		end
	end

	def update
		@field = current_user.fields.find_by_id params[:field][:id]
		if @field.update(field_params)
			#render json: {success: "Fields successfully updated"}
			flash[:success] = 'Fields successfully updated'
			# render 'new', layout: false
			render json: {success: "Updated field #{params[:field][:id]}"}
		else
			render json: {error: "Could not update field #{params[:field][:id]}"}
			# flash[:error] = 'Could not update field ' + params[:field][:id]
		end
	end

	private

	def default_field_type
		if (params[:field_type] == nil)
			params[:field_type] = ""
		end
	end

	def default_params
		params.require(:field).permit(:device_id)
			.merge(name: 'My Field')
	end

	def field_params
		params.require(:field)
			.permit(:id, :device_id, :name, :field_type)
	end

end
