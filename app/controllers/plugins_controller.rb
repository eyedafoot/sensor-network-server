class PluginsController < ApplicationController
	before_filter :authorize

	skip_before_action :verify_authenticity_token
	
	def index
		@breadcrumb = 'Global Plugins'
		@plugins = Plugin.where(is_protected: false)
	end

	def create
		@plugin = Plugin.new name: 'My Plugin', user_id: current_user.id, is_protected: true
		if @plugin.save
			redirect_to "/plugins/#{@plugin.id}/edit";
		else 
			redirect_to "/plugins";
		end
	end

	def show
		@plugin = Plugin.find_by_id params[:id]
		if @plugin
			@window_id = params[:window_id]
			node_name ="nodes_window_id_#{params[:window_id]}"
			gateway_name = "device_window_id_#{params[:window_id]}"
			cookies[:"#{node_name}"] = current_user.devices.where(device_type: "Node").to_json
			cookies[:"#{gateway_name}"] = current_user.devices.find_by_id(params[:device_id]).to_json
			render layout: false
		else
			render nothing: true
		end
	end

	def update
		@plugin = Plugin.find_by_id(params[:id])
		if @plugin.update plugin_params
			flash[:success] = "Plugin successfully updated"
			redirect_to edit_plugin_path(params[:id])
		else
			render 'edit'
		end
	end

	def edit
		@plugin = Plugin.find_by_id(params[:id]);
		@user_plugin_role = plugin_access_level(params[:id]);
	end

	def destroy
		if plugin_access_level(params[:id]) >= 4;
			windows = Window.find_by_plugin_id(params[:id]);
			if not windows.nil? && windows.delete(current_user)
				render json: {error: 'plugin is being used by a device you do not have access to - plugin can not be deleted', deleted:'N'}
			end
			plugin = Plugin.find (params[:id]);
			if plugin.delete
				render json: {success: 'plugin successfully deleted', deleted:'Y'}
			else
				render json: {error: 'plugin not deleted', deleted:'N'}
			end
		end
	end

	def tutorial
		render :action => 'tutorial'
	end

	def my_plugins
		@breadcrumb = 'My Plugins'
		@plugins = current_user.plugins
		render :'plugins/index'
	end

=begin
	This section if for plugin commands that users can use in their plugin js code.
	This will help simplify code for users.
=end
	def get_gateway
		window = current_user.windows.find(params[:window_id])
		gateway = Device.find(window.device_id);
		render json: gateway;
	end

	def get_nodes
		nodes = current_user.devices.where(device_type: "Node")
		render json: nodes;
	end

	def get_fields
		fields = current_user.fields.where(device_id: params[:node_id])
		render json: fields
	end

	def get_field_data
		begin
			field = Field.where("id in (?) AND device_id in (?)", params[:field_id].to_i, params[:node_id].to_i)
			if !field.nil? && !field.empty?
				require 'date'
				data = nil
				Datum.transaction do
					data = Datum.where(field_id: params[:field_id])
					data = data.where("id > ?", params[:last_id].to_i) if params[:last_id] != nil
					if params[:from_date] != nil
						from_date = nil
						from_date = DateTime.strptime(params[:from_date], '%m/%d/%Y %H:%M:%S') rescue from_date = nil
						from_date = DateTime.strptime(params[:from_date], '%m/%d/%Y') rescue from_date = nil if from_date == nil
						data = data.where("created_at >= ?", from_date) if from_date != nil
					end
					if params[:to_date] != nil
						to_date = nil
						to_date = DateTime.strptime(params[:to_date], '%m/%d/%Y %H:%M:%S') rescue to_date = nil
						to_date = DateTime.strptime(params[:to_date], '%m/%d/%Y') rescue to_date = nil if to_date == nil
						data = data.where("created_at <= ?", to_date) if to_date != nil
					end
					if params[:from_date] == nil && params[:to_date] == nil && params[:last_id] == nil
						data = data.order(id: :desc).limit(params[:num_samples])
					 	data = data.order(id: :asc)
					else
						data = data.limit(params[:num_samples]).order(id: :asc)
					end
				end
				if !data[0].nil?
					render json: data, :include => {:field => { :only => [:name]}}
				else
					render json: {error: 'No data found'};
				end
			else
				render json: {error: 'Field not found'};
			end
		rescue
			render json: {error: 'Error retrieving Data'}
		end
	end

	def get_gateway_commands
		commands = Command.find_by_device_id(params[:gateway_id]);
		if not commands.nil?
			render json: commands, :include => { :parameters => { :only => [:name, :value] } };
		else
			render json: {error: 'No commands found'};
		end
	end

	def create_gateway_command
		@command = Command.new(name: params[:name], priority: params[:priority], device_id: params[:gateway_id]);
		if !params[:parameters].nil?
			params[:parameters].each do |param|
				@command.parameters.new(name: param[1][:name], value: param[1][:value])
			end
			params.permit(:name, :priority)
		end
		if @command.save
			render json: { success: 'Command successfully saved'}
		else
			render json:{ error: 'Can not create command' }
		end
	end

	private 

	def plugin_params
		params.require(:plugin).permit(:name, :is_protected, :css, :javascript, :html, :type)
			.merge(user_id: current_user.id)
	end

	private

	#TODO: Setup proper access rights for plugins (RAED)
	def plugin_access_level(pluginId)
		begin
			plugin_exists = !(Plugin.where(id: pluginId, user_id: current_user.id).blank?);
			if plugin_exists
				return 4;
			else
				return 0;
			end
		end
	end
end
