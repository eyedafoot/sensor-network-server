class CommandsController < ApplicationController
	before_filter :authorize
	before_filter :require_gateway

	# TODO: DELETE THIS STATEMENT HERE ONCE PLUGIN AUTH FIXED
	skip_before_filter :verify_authenticity_token
	# ^^^ THIS ONE ^^^

	def new
		@command = Command.new
	end

	def create
		device_id = params[:gateway_id]
		if !params[:device_id].blank?
			device_id = params[:device_id]
		end
		@command = Command.new(name: params[:name], priority: params[:priority], device_id: device_id, active: params[:active])
		if !params[:names].nil?
			params[:names].zip(params[:values]).each do |name, value|
				@command.parameters.new(name: name, value: value)
			end
			params.permit(:name, :priority)
		end
		if @command.save
			render_command @command
		else 
			render_object({ error: 'Cannot create command' })
		end
	end

	def update
		links = DeviceToDeviceLink.where(parent:@gateway.id, child:current_user.access_rights.select(:device_id))
		device_ids = links.map(&:child)
		device_ids.push(@gateway.id)
		@command = Command.where(id: params[:id], device_id: device_ids).first()
		if @command.nil?
			render_object({ error: 'Command not found' })
		end
		if params[:name].nil?
			params[:name] = @command.name
		end
		if params[:device_id].nil?
			params[:device_id] = @command.device_id
		end
		if @command.update(device_id: params[:device_id], name: params[:name], priority: params[:priority], active: params[:active])
			render_command @command
		else
			render_object({ error: 'Cannot create command' })
		end
	end

	def show
		@command = Command.find_by_id(params[:id])
		if @command
			render json: @command, :include => { :parameters => { :only => [:name, :value] } }
		else 
			render json: { error: 'Command not found' }
		end 
	end

	def next
		@command = @gateway.next_command params
		render_command @command
	end

	def destroy
		links = DeviceToDeviceLink.where(parent:@gateway.id, child:current_user.access_rights.select(:device_id))
		device_ids = links.map(&:child)
		device_ids.push(@gateway.id)
		@command = Command.where(id: params[:id], device_id: device_ids).first()
		if @command.nil?
			render_object({ error: 'Command not found' })
		elsif @command.destroy
			render_object({ message: 'Command destroyed' })
		else 
			render_object({ error: 'Could not destroy command' })
		end
	end

	def execute
		links = DeviceToDeviceLink.where(parent:@gateway.id, child:current_user.access_rights.select(:device_id))
		device_ids = links.map(&:child)
		device_ids.push(@gateway.id)
		@command = Command.where(id: params[:id], device_id: device_ids).first()
		if @command
			@command.execute
			if @command.destroy
				render_object({ message: 'Command executed' })
			else
				render_object({ error: 'Could not destroy command' })
			end
		else
			render_object({ error: 'Command not found' })
		end
	end

	def create_param
		@parameter = Parameter.new(command_id: params[:command_id], name: params[:name], value: params[:value])
		if @parameter.save
			render_object @parameter
		else
			render_object({ error: 'Cannot create parameter' })
		end
	end

	def update_param
		links = DeviceToDeviceLink.where(parent:@gateway.id, child:current_user.access_rights.select(:device_id))
		device_ids = links.map(&:child)
		device_ids.push(@gateway.id)
		@parameter = Command.where(id: params[:command_id], device_id: device_ids).first().parameters.find(params[:id])
		if @parameter.nil?
			render_object({ error: 'Parameter not found' })
		end
		if params[:name].nil?
			params[:name] = @parameter.name
		end
		if @parameter.update(name: params[:name], value: params[:value])
			render_object @parameter
		else
			render_object({ error: 'Cannot create parameter' })
		end
	end

	def destroy_param
		links = DeviceToDeviceLink.where(parent:@gateway.id, child:current_user.access_rights.select(:device_id))
		device_ids = links.map(&:child)
		device_ids.push(@gateway.id)
		@parameter = Command.where(id: params[:command_id], device_id: device_ids).first().parameters.find(params[:id])
		if @parameter.nil?
			render_object({ error: 'Parameter not found' })
		elsif @parameter.destroy
			render_object({ message: 'Parameter destroyed' })
		else
			render_object({ error: 'Could not destroy parameter' })
		end
	end

	private

	def command_params
		if params[:names].nil?
			params.permit(:name, :priority)
		else
			params[:names].zip(params[:values]).each do |name, value|
				Parameter.new name: name[1], value: value[1]
			end
			params.permit(:name, :priority)
		end
	end

	def require_gateway
		@gateway = current_user.devices.find(params[:gateway_id])
		if @gateway.nil?
			if Gateway.find_by_id params[:gateway_id]
				render_object({ error: 'User does not have access to gateway' })
			else
				render_object({ error: 'Gateway does not exist' })
			end
		# elsif @gateway.type != 'Gateway'
		#  	render_object({ error: 'Gateway does not exist' })
		end
	end

	def render_command command
		if command.nil? 
			render nothing: true
		else
			respond_to do |format|
				format.json { 
					render json: command, :include => { :parameters => { :only => [:name, :value] } }
				}
				format.xml { 
					render xml: command, :include => { :parameters => { :only => [:name, :value] } }
				}
				format.html { 
					render json: command, :include => { :parameters => { :only => [:name, :value] } }
				}
			end
		end
	end

	def render_object object
		respond_to do |format|
			format.json { render json: object }
			format.xml { render xml: object }
			format.html { render json: object }
		end
	end

end
