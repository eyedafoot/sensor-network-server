class NetworkController < ApplicationController
	before_filter :authorize

	def index
		#thing = @devices[1].becomes(@devices[1].type.constantize)
		# Temp to show network diagrams
		@plugin = Plugin.find_by_id 1
		@devices = current_user.devices
		@nodes = @devices.where(device_type: "Node")
		@gateways = @devices.where(device_type: "Gateway")
	end

	def connect
		# Push get_configuration command to gateway queue
	end

	def update
		# Receive list of nodes connected to gateway with key x
		# Update nodes table to reflect current info
	end
end
