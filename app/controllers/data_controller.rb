class DataController < ApplicationController
	before_filter :nodes_field_exists

	def create
		@datum = Datum.new datum_params
		if @datum.save
			render json: @datum
		else
			render json: @datum.errors
		end
	end

	def index
		num_samples = params[:num_samples] || 5
		last_id = params[:last_id] || 0
		@data = @field.data.where("id > ?", last_id).order(:created_at, :id).last(num_samples.to_i)
		render json: @data
	end

	def export_data
		node = current_user.devices.find_by_id params[:node_id]
		field = node.fields.find_by_id params[:field_id]
		data = field.data;
		respond_to do |format|
			format.csv { send_data data.to_csv({headers: true}, %w{id field_id value created_at}) }
			format.xlsx { send_data (data.to_xlsx({headers: true}, %w{id field_id value created_at})).to_stream.read, :filename => "#{field.name}.xlsx" }
		end
	end

	private

	def datum_params
		params.permit(:field_id, :value)
	end

	def nodes_field_exists
		@node = current_user.devices.find_by_id params[:node_id]
		if @node.nil?
			if Device.find_by_id params[:node_id]
				render json: { error: 'User does not have access to node' }
			else
				render json: { error: 'Node does not exist' }
			end
		else
			@field = @node.fields.find_by_id params[:field_id]
			if @field.nil?
				if Field.find_by_id params[:field_id]
					render json: { error: 'User does not have access to field' }
				else
					render json: { error: 'Field does not exist' }
				end
			end
		end
	end
end
