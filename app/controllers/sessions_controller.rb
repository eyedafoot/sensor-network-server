class SessionsController < ApplicationController
	def new
		if current_user
			redirect_to '/network';
		end
	end

	def create
		user = User.find_by_username params[:session][:username]
		if user and user.authenticate params[:session][:password]
			# Save the user id inside the browser cookie.
			session[:user_id] = user.id
			redirect_to '/'
		else
			flash[:error] = 'Username or Password does not match.'
			@username = params[:session][:username]
			render 'new'
		end
	end

	def destroy
		session[:user_id] = nil
		redirect_to login_url
	end

	private
end
