class Mobileapi::DataController < Mobileapi::BaseController
  require 'json'

  before_filter :authenticate_master_key;
  before_filter :is_field, :only => [:create];

  def create
    if create_data(params[:field_id], params[:value])
      render_object({ success: 'Data Saved' });
    else
      render_object({ error: 'Save Failed' });
    end
  end

  def create_mult
    respond_to do |format|
      format.json { render json: mult_data_json unless performed?}
      format.xml { render xml: mult_data_xml unless performed?}
    end
  end

  def create_via_file
    respond_to do |format|
      format.json { render json: mult_data_file unless performed?}
      format.xml { render xml: mult_data_file unless performed?}
    end
  end

  private

  def mult_data_json
    begin
      success = true;
      Datum.transaction do
        #Datum.create(JSON.parse(params[:data]))
        # create_mult_data(JSON.parse(params[:data]))
        create_mult_data(params[:data])
      end
      if success
        return { success: 'Data Saved' };
      else
        return{ success: 'Save Failed for one or more data entries' };
      end
    rescue
      return { success: 'Error occurred formatting json post' }
    end
  end

  def mult_data_xml
    begin
      success = true;
      Datum.transaction do
        #Datum.create(Hash.from_xml(params[:data]))
        create_mult_data(Hash.from_xml(params[:data]))
      end
      if success
        return { success: 'Data Saved' };
      else
        return{ success: 'Save Failed for one or more data entries' };
      end
    rescue
      return { success: 'Error occurred formatting xml post' }
    end
  end

  def mult_data_file
    file_data = params[:sendfile];
    if file_data.respond_to?(:read)
      content = file_data.read;
      content_array = content.split(/,/);

      data_array = [];

      i = 0;
      while i < content_array.length
        data = {field_id:content_array[i], value:content_array[i+1]};
        data_array.push(data);
        i += 2;
      end
      if data_array.size > 0
        create_mult_data(data_array);
      end
      return{ success: 'Data read from file - read method' };
    elsif file_data.respond_to?(:path)
      content = file_data.path;
      return{ success: 'Data read from file - path method' };
    else
      logger.error "bad file uploaded"
      return {error: 'Error: unable to read file'}
    end
  end

  def create_data(field_id, value)
    datum = Datum.new
    datum.field_id = field_id;
    datum.value = value;
    if datum.save
      true;
    else
      false;
    end
  end

  def create_mult_data(data_hash)
    if data_hash.nil? || data_hash.empty?
      #Do nothing
    else
      if data_hash[0]['field_id'].nil? && !data_hash[0][:field_id].nil?
        values = data_hash.map { |data| "(#{data[:field_id]},#{data[:value]})" }.join(",")
        ActiveRecord::Base.connection.execute("INSERT INTO data (field_id, value) VALUES #{values}")
      elsif data_hash[0]['created_at'].nil?
        values = data_hash.map { |data| "(#{data['field_id']},#{data['value']})" }.join(",")
        ActiveRecord::Base.connection.execute("INSERT INTO data (field_id, value) VALUES #{values}")
      else
        values = data_hash.map { |data| "(#{data['field_id']},#{data['value']},'#{DateTime.parse(data['created_at']).to_s(:db)}')" }.join(",")
        ActiveRecord::Base.connection.execute("INSERT INTO data (field_id, value, created_at) VALUES #{values}")
      end
    end
  end
end
