class Mobileapi::CommandController < Mobileapi::BaseController
  before_filter :authenticate_master_key;
  before_filter :is_gateway;

  def show
    render_next_command params
  end

  def next
    is_command
    # begin
      gateway = @mobile_user.devices.find(params[:gateway_id])
      links = DeviceToDeviceLink.where(parent:gateway.id)#,child:current_user.access_rights.select(:device_id))
      device_ids = links.map(&:child)
      device_ids.push(gateway.id)
      command = Command.where(id: params[:command_id], device_id: device_ids).first()
      if command
        command.destroy
      end
    # rescue
    #   render_object({error: 'An Error occurred - Deleting command or Retrieving next command'})
    #   return
    # end
    render_next_command params
  end

  private

  def render_next_command params
    gateway = @mobile_user.devices.find(params[:gateway_id])

    nodes = []
    begin
      links = DeviceToDeviceLink.where(parent:gateway.id)#,child:current_user.access_rights.select(:device_id))
      links.each do |link|
        node = Device.find(link.child)
        nodes.push(node)
      end
    end

    commands = []
    begin
      nodes.each do |node|
        node_commands = node.commands.where(active: true).order(priority: :asc, created_at: :asc)
        if node_commands
          node_commands.each do |command|
            commands.push(command)
          end
        end
      end
      gateway_commands = gateway.commands.where(active: true).order(priority: :asc, created_at: :asc)
      gateway_commands.each do |command|
        commands.push(command)
      end
    end
    commands = commands.sort_by{|c| [c.priority, c.created_at] }
    if commands
      command = commands.first
    else
      command = nil
    end
    if command
      respond_to do |format|
        format.json { render json: command, :include => { :parameters => { :only => [:name, :value] } } unless performed?}
        format.xml { render xml: command, :include => { :parameters => { :only => [:name, :value] } } unless performed?}
      end
    else
      render_object({ error: 'Command not found' })
    end
  end
end
