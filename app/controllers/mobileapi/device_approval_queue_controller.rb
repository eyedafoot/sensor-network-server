class Mobileapi::DeviceApprovalQueueController < Mobileapi::BaseController
  before_filter :authenticate_master_key;
  before_filter :is_gateway, :only => [:device_approval_request, :registered_devices];
  before_filter :check_address, :only => [:device_approval_request]


  def device_approval_request
    if DeviceApprovalQueue.find_by_address(params[:address]).nil? && Device.find_by_address(params[:address]).nil?
      daq = DeviceApprovalQueue.new(device_id: params[:gateway_id], address: params[:address])
      if daq.save
        render_object({success: "Successfully queued devices for registration"});
      else
        render_object({error: "Unable to queue device for registration"});
      end
    else
      render_object({error: "Device already in queue for registration"});
    end
  end

  def registered_devices
    render_devices(params)

    #TODO: return device list with address
    #TODO: put rejected flag on list item to say the user rejected it so it doesn't keep asking to join
  end

  private

  def render_devices params
    begin
      linked_devices = DeviceToDeviceLink.where(parent: params[:gateway_id])
      if linked_devices
        device_ids = Array.new
        linked_devices.each do |linked_device|
          device_ids.push(linked_device.child)
        end
        devices = Device.all.where(id: device_ids).where(device_type: 'Node').where.not(address: nil)
        if devices.nil? || devices.empty?
          render_object({ error: 'No devices found' })
        else
          respond_to do |format|
            format.json { render json: devices, :only => [:id, :node_id, :address], :include => { :fields => { :only => [:field_type, :id] } } unless performed?}
            format.xml { render xml: devices, :only => [:id, :node_id, :address], :include => { :fields => { :only => [:field_type, :id] } } unless performed?}
          end
        end
      else
        render_object({ error: 'No devices found' })
      end
    rescue
      render_object({ error: 'Error retrieving devices' })
    end
  end

  def check_address
    if params[:address].nil?
      render_object({error: "Error 'address' parameter is missing"})
    end
  end
end