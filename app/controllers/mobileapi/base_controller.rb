class Mobileapi::BaseController < ApplicationController
  skip_before_filter :verify_authenticity_token

  private
  def authenticate_master_key
    begin
      master_key = Key.find_by_value params[:master_key];
      if master_key
        @mobile_user = User.find_by_id(master_key.user_id)
        if @mobile_user.nil?
          render_object({ error: 'No user with Master Key not found' });
        end
      else
        render_object({ error: 'Master Key not found' });
      end
    rescue
      render_object({error: 'An Error occurred - Master Key not found'});
    end
  end

  #TODO: check if node by key
  def is_node
    begin
      node = Device.where(id: params[:node_id], device_type: 'Node')
      if node.nil? || node.empty?
        render_object({ error: 'Node not found' });
      else
        @node = node;
      end
    rescue
      render_object({error: 'An Error occurred - Node not found'});
    end
  end

  #TODO: check if gateway by key
  def is_gateway
    begin
      gateway = Device.where(id: params[:gateway_id], device_type: 'Gateway')
      if gateway.nil? || gateway.empty?
        render_object({ error: 'Gateway not found' });
      else
        @gateway = gateway;
      end
    rescue
      render_object({error: 'An Error occurred - Gateway not found'})
    end
  end

  def is_field
    begin
      field = Field.find(params[:field_id]);
      if field
        @field = field;
      else
        render_object({ error: 'Field not found' });
      end
    rescue
      render_object({error: 'An Error occurred - Field not found'});
    end
  end

  def is_command
    begin
      command = Command.find(params[:command_id])
      if command
        @command = command;
      else
        render_object({ error: 'Command not found' });
      end
    rescue
      render_object({error: 'An Error occurred - No command Found'});
    end
  end

  def user_access_node

  end

  def user_access_gateway
    begin
      @gateway = current_user.gateways.find(params[:gateway_id])
      if @gateway.nil?
        if Gateway.find(params[:gateway_id])
          render_object({ error: 'User does not have access to gateway' });
        else
          render_object({ error: 'Gateway does not exist' });
        end
      end
    rescue
      render_object({error: 'An Error occurred - Access Denied'});
    end
  end

  def user_access_field

  end

  def render_object object
    respond_to do |format|
      format.json { render json: object unless performed?}
      format.xml { render xml: object unless performed?}
      format.html { render json: object unless performed?}
    end
  end
end
