class SettingsController < ApplicationController
  before_filter :authorize

  def index

  end

  def picklist
    @settings = Setting.where(item_code: params[:item_code])
    @item_code = params[:item_code]
    render :action => '/picklist'
  end

  def update
    id = params[:id].to_i

    begin
      setting = Setting.where(id:id)
      if setting.empty?
        setting = Setting.new item_code:params[:item_code], item_description:params[:item_description]
      else
        setting.item_code = params[:item_code]
        setting.item_description = params[:item_description]
      end

      if setting.save
        render json: {success: "Successfully saved setting", setting: setting}
      else
        render json: {error: "Unabled to save setting"}
      end
    rescue
      render json: {error: "An error occurred trying to update setting"}
    end
  end

  def destroy
    setting = Setting.find(params[:id])
    if setting.destroy
      render json: {success: "Successfully deleted setting"}
    else
      render json: {error: "Error occurred deleting setting"}
    end
  end
end
