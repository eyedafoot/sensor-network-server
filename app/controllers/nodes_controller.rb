class NodesController < ApplicationController
	before_filter :authorize

	def show
		@node = current_user.devices.find_by_id params[:id]
		if @node
			@plugins = Plugin.where(type: :Node)
				.where(is_protected: false)
				.concat(current_user.plugins.where(type: :Node))
				.uniq
			@windows = @node.windows.order(:index)
			@fields = @node.fields

			settings = Setting.where(item_code: 'nodetype')
			@field_types = []
			if !settings.nil? && !settings.empty?
				settings.map do |setting|
					@field_types << setting.item_description
				end
			end
		elsif Node.find_by_id params[:id]
			flash[:error] = 'User does not have access rights to the node'
			redirect_to network_path
		else
			flash[:error] = 'Node does not exist'
			redirect_to network_path
		end
	end

end
