class UsersController < ApplicationController

	def new
		@user = User.new
	end

	def create
		flash[:error] = "TODO - Setup Error message to use this format";
		@user = User.new user_params
		@user.privilege_level = (Privilege.find_by_name 'SystemAdmin').id
		if @user.save
			session[:user_id] = @user.id
      key = Key.generate_key
      key.user_id = @user.id
      key.save
			redirect_to '/'
		else 
			render 'new' 
		end
	end

	def destroy
		if is_system_admin
			begin
				Key.destroy.where(user_id: params[:id])
				User.destroy(params[:id])
			rescue
				flash[:error] = "Error occurred attempting trying to delete user."
			end
		end
		redirect_to users_list_path
	end

	def edit
		if is_system_admin
			@user = User.find(params[:id]);
			@privileges = Privilege.order('id ASC');
		end
	end

	def update
		if is_system_admin
			@user = User.find(params[:id])
			if @user.update(params.require(:user).permit(:email, :privilege_level))
				flash[:success] = "#{@user.username} successfully updated"
				redirect_to users_list_path
			else
				flash[:error] = 'Error occurred while attempting to update user information'
				redirect_to edit_user_path(params[:id])
			end
		else
			flash[:alert] = 'You do not have access to edit this user'
			redirect_to edit_user_path(params[:id])
		end
	end

	def user_list
		if is_admin
			@users = User.order('created_at DESC')
			render users_list_path
		end
	end

	private

	def user_params
		params.require(:user).permit(:email, :username, :password, :password_confirmation)
	end

	def get_user
		@user = User.find params[:id]
	end

end
