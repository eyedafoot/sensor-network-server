class AccessRightsController < ApplicationController
	before_filter :authorize
	
	def index
		@rights = current_user.access_rights
	end
	
	def create
		@rights = current_user.access_rights
		key = Key.find_by_value params[:key][:value]
		if key and key.device_id
			right = AccessRight.new user_id: current_user.id, device_id: key.device_id
			if right.save
				redirect_to access_rights_path
			else
				flash[:error] = 'Could not save access right.'
				render 'index'
			end
		else
			flash[:error] = 'Device does not exist. Please check your key.'
			render 'index'
		end
	end

	def destroy
		right = AccessRight.find(params[:id])
		if right.destroy
			redirect_to access_rights_path
		else
			flash[:error] = 'Could not destroy access right.'
			render 'index'
		end
	end
end
