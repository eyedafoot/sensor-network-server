
$( document ).ready(function() {
    gatewayConnect = $("#gateway-connect");
    selectNode = $("#node-select");
    nodeConfig = $("#node-configuration");
    nodeConfigStatus = $("#node-config-status");
    adcTable = nodeConfig.find("table[name=adc]");
    tempTable = nodeConfig.find("table[name=temperature]");
    accTable = nodeConfig.find("table[name=accelerometer]");
    addAdcConfig = adcTable.find("a[name=add-config]");
    adcConfigTemplate = adcTable.find("tr[name=configuration-template]");

    /*var requiredVarToPlaceholder = { gatewayId: 0 };
     var winVars = getResourceVars("window", "gatewayId");

     setupWindowVariables(winVars, requiredVarToPlaceholder);	*/

    var windowId = $('#windowBody').attr('data-id');

    var gateway = getDevice(windowId);

    var nodeOptions = "";
    var nodes = getNodes(windowId);
    for (var i = 0 ; i < nodes.length ; i++) {
        nodeOptions += "<option value='"+ nodes[i].node_id +"'>"+ nodes[i].name +"</option>";
    };
    $('#node-select').append(nodeOptions);

////// START OF CONNECT TO NETWORK FUNCTIONS //

    // Get the network configuration by creating a "Get_Node_Configuration" command.
    $("#connect").click(function (e) {
        e.preventDefault();

        path = "/commands.json";
        post = "gateway_id=" + gateway.id;
        post = post + "&name=Get_Node_Configuration&priority=1";

        // Show the status
        selectNode.val("null").change();
        nodeConfigStatus.html("Loading...");
        nodeConfigStatus.attr("class", "config-status");
        nodeConfigStatus.show("fast");

        // Send the Get_Node_Configuration command
        $.ajax({
            url: path,
            type: "POST",
            data: post,
            success: function(command) {
                if (command.error != null) {
                    setNodeConfigStatus("Connection failed!<br/><br/>"+ "Error:<br/>" + command.error, false);
                } else {
                    nodeConfigStatus.html("");
                    gatewayConnect.hide();

                    nodeConfigStatus.hide("fast");
                    selectNode.show("fast");

                    // nodeConfigStatus.html("Loading...");
                    // nodeConfigStatus.attr("class", "config-status");
                    // nodeConfigStatus.show("fast");
                    // setConfigsFromResponse(new Date(), 5);
                }

            },
            fail: function(response,status) {
                setNodeConfigStatus("Connection failed!<br/><br/>" + "Error:<br/>"
                    + "Could not create command (fail status).", false);
            }
        });

    });

    // Receive the list of nodes & their configurations from the gateway
    // Stops when time between configurations is greater than timeout
    function setConfigsFromResponse(startTime, secTimeout, configs) {
        var configs = configs || [];
        $.ajax({
            url: "/commands/next.json?gateway_id=" + gateway.id + "&name=Get_Node_Configuration",
            type: "GET",
            success: function(command) {
                if (command.error) {
                    setNodeConfigStatus("Configuration failed!<br/><br/>" + "Error:<br/>" + command.error, false);
                } else {
                    if (secondsFrom(startTime) < secTimeout) {
                        if (command) {
                            configs.push(command);
                            setTimeout(function() {
                                setConfigsFromResponse(new Date(), secTimeout, configs)
                            }, 1000);
                        } else {
                            setTimeout(function() {
                                setConfigsFromResponse(startTime, secTimeout, configs)
                            }, 1000);
                        }
                    } else {
                        selectNode.empty();
                        if (configs.length) {
                            for (var config in configs) {
                                selectNode.append("<option value=" + config.parameters.node
                                    + ">Node " + config.parameters.node
                                    + "</option>");
                                // TODO: Create cookie for diagram
                            }
                            nodeConfigStatus.hide("fast");
                            selectNode.show("fast");
                        } else {
                            setNodeConfigStatus("No nodes connected<br/><br/>", false);
                        }
                    }
                }
            },
            fail: function() {
                setNodeConfigStatus("Configuration failed!<br/><br/>" + "Error:<br/>"
                    + "Unable to check command (fail status).", false);
            }
        });
    }

    // END OF CONNECT TO NETWORK FUNCTIONS //


////// START CONFIG FUNCTIONS //

    // Default adc config
    addAdcConfigToForm();

    // When selected node changes
    selectNode.change(function(e) {
        // Hide config when no nodes selected
        if (selectNode.find(":selected").val() == "null") {
            nodeConfig.hide("fast");
        } else {
            // Hide status and show node config
            nodeConfigStatus.hide("fast");
            nodeConfig.show("fast");
        }
        // Uncheck all boxes
        $('input[type=checkbox]').each(function() {
            $(this).prop('checked', false).change();
        });
    });

    // Checkboxes toggle config form
    $(".config-toggle").change(function(e) {
        if (this.checked) {
            $("table[name="+ this.name +"]").show("fast");
        } else {
            $("table[name="+ this.name +"]").hide("fast");
        }
    });

    // When adc mode changes
    adcTable.find("select[name=mode]").change(function(e) {
        adcTable.find("tr[name=configuration]").each(function(i) {
            $(this).remove();
        });
        // Change how add is display depending on mode
        mode = adcTable.find("select[name=mode]").find(":selected").val();

        // Single config
        if ((mode == 0) || (mode == 2)) {
            addAdcConfig.hide("fast");
            // Multi config
        } else {
            addAdcConfig.show("fast");
        }

        // Periodic
        if ((mode == 0) || (mode == 1)) {
            adcTable.find("tr[name=seconds]").show("fast");
            adcTable.find("tr[name=clock]").hide("fast");
            // RMS
        } else {
            adcTable.find("tr[name=seconds]").show("fast");
            adcTable.find("tr[name=clock]").show("fast");
        }

        addAdcConfigToForm(); // Default adc config
    });

    // When "add" adc config link clicked
    addAdcConfig.click(function(e) {
        addAdcConfigToForm();
    });

    function addAdcConfigToForm() {
        // Insert a config before add link
        adcConfig = adcConfigTemplate.clone();
        adcConfig.insertBefore(addAdcConfig.closest("tr"));
        adcConfig.removeAttr("style");
        adcConfig.attr("name", "configuration");
        numConfigs = adcTable.find("tr[name=configuration]").length;

        // If is 4th adc config then hide add link and add remove listener
        if (numConfigs >= 4) {
            addAdcConfig.hide("fast");
            adcConfig.find(".btn-link").click(function(e) {
                removeAdcConfig(e);
            });
            // If is 1st adc config then hide remove link
        } else if (numConfigs <= 1) {
            adcConfig.find(".btn-link").hide();
            // If there is 2-4 adc configs then add remove link listener
        } else {
            adcConfig.find(".btn-link").click(function(e) {
                removeAdcConfig(e);
            });
        }
    }

    function removeAdcConfig(e) {
        e.target.closest("tr[name=configuration]").remove();
        numConfigs = adcTable.find("tr[name=configuration]").length;

        // If less than 4 configs, then show add link
        if (numConfigs < 4) {
            addAdcConfig.show("fast");
        }
    }

    // END CONFIG FUNCTIONS //


////// START OF SENDING FUNCTIONS //

    // Parse the config form into a create command URL
    // Once done, send the request, and wait for response
    $("#send-config").click(function (e) {
        e.preventDefault();

        path = "/commands.json";
        post = "gateway_id=" + gateway.id +"&name=Set_Node_Configuration&priority=1";
        post = post + "&names[]=node" + "&values[]=" + selectNode.find(":selected").val();


        if (adcTable.is(":visible")) {
            post = post + "&names[]=adc_mode" + "&values[]=" + adcTable.find("select[name=mode]").find(":selected").val();
            post = post + "&names[]=adc_bits" + "&values[]=" + adcTable.find("input[name=bits]").val();

            if (adcTable.find("select[name=clock]").is(":visible")) {
                post = post + "&names[]=adc_clock" + "&values[]=" + adcTable.find("select[name=clock]").find(":selected").val();
            } else {
                post = post + "&names[]=adc_seconds" + "&values[]=" + adcTable.find("input[name=seconds]").val();
            }

            adcTable.find("tr[name=configuration]").each(function(i) {
                post = post + "&names[]=adc_cfg" + i + "_positive" + "&values[]=" + $(this).find("select[name=positve]").find(":selected").val();
                post = post + "&names[]=adc_cfg" + i + "_negative" + "&values[]=" + $(this).find("select[name=negative]").find(":selected").val();
            });
        }
        if (tempTable.is(":visible")) {
            post = post + "&names[]=temp_seconds" + "&values[]=" + tempTable.find("input[name=seconds]").val();
        }
        if (accTable.is(":visible")) {
            post = post + "&names[]=acc_maxG" + "&values[]=" + accTable.find("select[name=max-g]").find(":selected").val();
            post = post + "&names[]=acc_seconds" + "&values[]=" + accTable.find("input[name=seconds]").val();
        }

        // Hide configuration
        selectNode.val("null").change();
        nodeConfigStatus.html("Loading...");
        nodeConfigStatus.attr("class", "config-status");
        nodeConfigStatus.show("fast");

        // Create the Set_Node_Configuration command
        $.ajax({
            url: path,
            type: "POST",
            data: post,
            success: function(command) {
                if (command.error == null) {
                    waitForCommandToExecute(command.id, new Date(), 60);
                } else {
                    setNodeConfigStatus("Configuration failed!<br/><br/>"+ "Error:<br/>" + command.error, false);
                }
            },
            fail: function(response,status) {
                setNodeConfigStatus("Configuration failed!<br/><br/>" + "Error:<br/>"
                    + "Could not create command (fail status).", false);
            }
        });
    });

    // Continuously check the command until timeout is reach or the command is executed.
    // May also exit if an error occurs in the request or the response.
    // COMMAND IS DELETED ON TIMEOUT
    function waitForCommandToExecute(commandId, startTime, secTimeout) {
        $.ajax({
            url: "/commands/" + commandId + ".json?gateway_id=" + gateway.id,
            type: "GET",
            success: function(command) {
                if (command.error) {
                    setNodeConfigStatus("Configuration failed!<br/><br/>" + "Error:<br/>" + command.error, false);
                } else if (command.executed_at != null) {
                    setNodeConfigStatus("Configuration successful!<br/><br/>", true);
                } else {
                    if (secondsFrom(startTime) < secTimeout) {
                        setTimeout(function() {
                            waitForCommandToExecute(commandId, startTime, secTimeout)
                        }, 1000);
                    } else {
                        setNodeConfigStatus("Configuration failed!<br/><br/>" + "Error:<br/>"
                            + "Wait for execute timeout after "+secTimeout+" seconds.", false);
                        deleteCommand(command.id);
                    }
                }
            },
            fail: function() {
                setNodeConfigStatus("Configuration failed!<br/><br/>" + "Error:<br/>"
                    + "Unable to check command (fail status).", false);
            }
        });
    }

    // END OF SENDING FUNCTIONS //


////// START OF MISC FUNCTIONS //

    function secondsFrom(startTime) {
        timeDiff = new Date() - startTime;
        timeDiff /= 1000; // strip the ms
        timeDiff = Math.round(timeDiff % 60); // seconds
        return timeDiff;
    }

    function setNodeConfigStatus(html, isSuccess) {
        nodeConfigStatus.html(html);
        if (isSuccess) {
            nodeConfigStatus.attr("class", "config-status success");
        } else {
            nodeConfigStatus.attr("class", "config-status fail");
        }
    }

    function deleteCommand(commandId) {
        $.ajax({
            url: "/commands/" + commandId + "?gateway_id=" + gateway.id,
            type: "DELETE",
            success: function(command) {
                nodeConfigStatus.html(nodeConfigStatus.html() + " Failed configuration deleted.", false);
            },
            fail: function(response,status) {
                nodeConfigStatus.html(nodeConfigStatus.html() + " Unabled to delete failed configuration.", false);
            }
        });
    }

    // END OF MISC FUNCTIONS //


});