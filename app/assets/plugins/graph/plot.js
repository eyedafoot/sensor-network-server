var numSamples = 5;
var numTick = 5;
var mT, mB, mL, mR;
var w, h, plot, lineGen, line, clip, circles, group;
var xScale, xAxis, xContainer;
var yScale, yAxis, yContainer;
var title, xLabel, yLabel;

$(document).ready(function () {

	var requiredVarToPlaceholder = { 
		nodeId: 0, 
		fieldId: 0
	};

	var optionalVarToDefault = { 
		title: "", 
		xLabel: "", yLabel: "", 
		numSamples: 5, timeDependent: true 
	};

	var winVars = getResourceVars("window", $("body").data("id"));

	if (setupWindowVariables(winVars, requiredVarToPlaceholder, optionalVarToDefault)) {
		var data = new Array();

		initPlot(winVars);
		
		updateData(data, winVars);
		setInterval(function () { 
			updateData(data, winVars);
		}, 5000);
	} 
});

function updateData(data, winVars) {
	var params = "field_id=" + winVars.fieldId + "&node_id=" + winVars.nodeId + "&num_samples=" + numSamples;
	if (data.length) {
		params = params + "&last_id=" + data[data.length - 1].id;
	}
	 
	$.ajax({
		url: "/data?" + params,
		type: "GET",
		success: function(samples) {
			if (samples.error) {
				addError(samples.error);
				$("#data-plot").hide();
			} else if (samples.length) {
				samples.forEach(function (el) {
					if (winVars.timeDependent == true) {
						data.push({ x: new Date(el.updated_at), y: el.value, id: el.id });
					} else {
						data.push({ x: el.id, y: el.value, id: el.id });
					}
				});

				// newData excludes depreciated values
				if (data.length > numSamples) {
					newData = data.slice();
					newData.shift();
				} else {
					newData = data;
				}

				updatePlot(data, newData);
			}
		},
		fail: function(response,status) {
			addError(response);
			$("#data-plot").hide();
		}
	});
}

function updatePlot(data, newData) {
	// x scales to only new data, while 
	// y scales to new + depreciated (stops vertical tearing at 0)
	var x = newData.map(function(datum) {return datum.x;});
	var y = data.map(function(datum) {return datum.y;});

	// Get new x and y scales 
	xScale
		.domain([d3.min(x), d3.max(x)])
		.range([mL, w-mR]);

	yScale
		.domain([d3.min(y), d3.max(y)])
		.range([h-mB, mT]);

	xAxis
		.ticks(numTick)
		.scale(xScale)

	yAxis
		.ticks(numTick)
		.scale(yScale)
		.orient("left");

	lineGen
		.x(function(d) { return xScale(d.x); }) 
		.y(function(d) { return yScale(d.y); })
		.interpolate("linear");

	var amt = xScale(newData[0].x) - xScale(data[0].x);

	// Call the new axis
	xContainer.call(xAxis);
	yContainer.call(yAxis);

	circles = group.selectAll("circle")
		.data(data);

	circles.enter()
		.append("circle")
		.attr("r", "5")
		.attr("fill", "red")
		.attr("cx", function (d) { return xScale(d.x); })
		.attr("cy", function (d) { return yScale(d.y); });

	if (data.length > numSamples) {
		line
			.attr("d", lineGen(data))

		circles
			.attr("cx", function (d) { return xScale(d.x); })
			.attr("cy", function (d) { return yScale(d.y); });

		// Draw the new path with the current + depreciated data transformed by amt
		// Transition to 0 to Scale new data to new x scale
		group
			.attr("transform", "translate("+amt+")")
			.transition()
			.attr("transform", "translate(0)");

		data.shift();
	} else {
		line
			.attr("transform", "")
			.transition()
			.attr("d", lineGen(newData));

		circles
			.transition()
			.attr("cx", function (d) { return xScale(d.x); })
			.attr("cy", function (d) { return yScale(d.y); });
	}

	circles.exit().remove();
}

function initPlot(winVars) {
	lineGen = d3.svg.line();
	if (winVars.timeDependent == true) {
		xScale = d3.time.scale();
	} else {
		xScale = d3.scale.linear();
	}
	xAxis = d3.svg.axis();
	yScale = d3.scale.linear();
	yAxis = d3.svg.axis();

	// margins
	if (winVars.title) mT = 50; else mT = 30;
	if (winVars.xLabel) mB = 60; else mB = 40;
	if (winVars.yLabel) mL = 60; else mL = 40;
	mR = 40;

	plot = d3.select("#data-plot")
		.append("svg:svg")
		.attr("style", "height: 100%; width: 100%;")
		.append("svg:g");

	w = $("#data-plot").width();
	h = $("#data-plot").height();

	xContainer = plot.append("svg:g")
		.attr("class", "x-axis")
		.attr("transform", "translate(0,"+ (h-mB) +")");

	yContainer = plot.append("svg:g")
		.attr("class", "y-axis")
		.attr("transform", "translate(" + mL + ",0)");

	clip = plot.append("defs").append("svg:clipPath")
		.attr("id", "clip")
		.append("svg:rect")
		.attr("id", "clip-rect")
		.attr("x", mL)
		.attr("y", mT-10)
		.attr("width", w-mR)
		.attr("height", h-mB+10);

	group = plot.append("svg:g")
		.attr("clip-path", "url(#clip)")
		.append("svg:g");

	line = group.append("svg:path")
		.attr("stroke", "black")
		.attr("stroke-width", "2")
		.attr("id", "line-path")
		.attr("fill", "none");

	circles = group.selectAll("circle");

	title = plot.append("text")
		.attr("x", w/2)
		.attr("y", mT/2)
		.style("text-anchor", "middle")
		.text(winVars.title);

	xLabel = plot.append("text")
		.attr("x", w/2)
		.attr("y", h-mB/4)
		.style("text-anchor", "middle")
		.text(winVars.xLabel);

	yLabel = plot.append("text")
		.attr("transform", "rotate(-90)")
		.attr("x", -h/2)
		.attr("y", mL/3)
		.style("text-anchor", "middle")
		.text(winVars.yLabel);
}