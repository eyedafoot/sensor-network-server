$(document).ready(function(){
    var printHere = $('#printHere');

    var windowId = $('#windowBody').data('id');
    printHere.append('windowId: '+ JSON.stringify(windowId)+'<br />');

    var gateway = getGateway(windowId);
    printHere.append('gateway: '+ JSON.stringify(gateway)+'<br />');

    var nodes = getNodes(windowId);
    printHere.append('nodes: '+ JSON.stringify(nodes)+'<br />');

    var param = Object.create(Parameter);
    param.init('paramVar','value');
    printHere.append('parameter: '+ JSON.stringify(param)+'<br />');

    var command = Object.create(Command);
    command.init('name',1);
    printHere.append('command: '+ JSON.stringify(command)+'<br />');

    command.setParameter('name 1', 'value 1');
    printHere.append('command: '+ JSON.stringify(command)+'<br />');

    command.setParameters(param);
    printHere.append('command: '+ JSON.stringify(command)+'<br />');

    var command2 = Object.create(Command);
    command2.init('name',1, param);
    printHere.append('command2: '+ JSON.stringify(command)+'<br />');

    var paramArray = [param,param];
    command.setParameters(paramArray);
    printHere.append('command: '+ JSON.stringify(command)+'<br />');

    command.addParameter('newParam','val');
    printHere.append('command: '+ JSON.stringify(command)+'<br />');

    command.addParameters(param);
    printHere.append('command: '+JSON.stringify(command)+'<br />');

    command.addParameters(paramArray);
    printHere.append('command: '+JSON.stringify(command)+'<br />');

    var data = getNodeFieldData(nodes[0].id, 1, null, null);
    printHere.append('data for node 1: '+ JSON.stringify(data)+'<br />');

    var commands = getGatewayCommands(gateway.id);
    printHere.append('commands for gateway 1: '+ JSON.stringify(commands)+'<br />');

    var createCommand = createGatewayCommand(gateway.id, command);
    printHere.append('Saving command for gateway 1: '+ JSON.stringify(createCommand)+'<br />');

    var commands = getGatewayCommands(gateway.id);
    printHere.append('commands for gateway 1: '+ JSON.stringify(commands)+'<br />');
});