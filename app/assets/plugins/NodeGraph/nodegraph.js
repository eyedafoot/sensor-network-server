var windowId = -1;
var deviceId = -1;
var fieldId = -1;
var numSamples = 50;
var maxNumSamplesToDisplay = 250;
var data = [];
var fields;

var fromDate;
var toDate;

var chart;
var refreshSeconds = 10;

$(document).ready(function(){
    windowId = $('#windowBody').data('id');
    var device = getDevice(windowId);
    deviceId = device.id;
    fields = getFields(deviceId);

    chart = c3.generate({
        data: {
            /*
             x: 'date',
             xFormat: '%Y-%m-%d %H:%M:%S', */
            columns: []
        },
        axis: {
            x: {
                /*type: "timeseries",
                 localtime: false,
                 tick: {
                 format: '%Y-%m-%d %H:%M:%S'
                 },
                 height: 30*/
            },
            y : {
                tick: {
                    format: function(d){return d3.round(d,6);}
                }
            }
        },
        subchart: {
            show: true
        },
        zoom: {
            enabled: true
        },
        legend: {
            position: 'inset'
        },
        tooltip: {
            grouped: false
        }
    });

    $('#fromDate').datetimepicker({
        timeFormat: 'HH:mm:ss z',
        stepSecond: 10
    }).change(function(){
        fromDate = $("#fromDate").val();
        data = [];
    });

    $('#toDate').datetimepicker({
        timeFormat: 'HH:mm:ss',
        stepHour: 1,
        stepMinute: 1,
        stepSecond: 10
    }).change(function(){
        toDate = $("#toDate").val();
        data = [];
    });

    $("#fieldIds").multiselect({
        includeSelectAllOption: true,
        onChange: function(element, checked) {
            getData(false);
        },
        buttonWidth: '100%'
    });

    fieldOptions = getFieldOptions(fields);
    $("#fieldIds").multiselect('dataprovider', fieldOptions);

    getData(true);
});

function getData(timeout) {
    $('#fieldIds option:selected').each(function(){
        fieldId = $(this).val();
        //get current data for selected field
        let fieldName = $(this).text();
        var currentData = data[fieldName];
        newData = getNodeFieldData(deviceId, fieldId, numSamples, currentData, fromDate, toDate);
        if (currentData == null || currentData.length <= 0) {
            currentData = [];
        }
        if (currentData != null) {
            if (newData != null && newData.length > 0) {
                //format new data
                newData = formatData(newData, fieldName);
                $.each(newData, function(i, item){
                    newData[i].created_at = Date.parse(newData[i].created_at);
                });
                currentData = currentData.concat(newData);
                if(currentData.length > maxNumSamplesToDisplay) {
                    var numToRemove = currentData.length - maxNumSamplesToDisplay;
                    currentData = currentData.slice(numToRemove, currentData.length);
                }
                data[fieldName] = currentData;
                newData = null;
                chart.load({
                    json: currentData,
                    keys: {
                        /*x: 'created_at',*/
                        value: [ fieldName ]
                    },
                    duration: 1500
                });
            }
        }
    });
    $('#fieldIds option:not(:selected)').each(function(){
        data[$(this).text()] = null;
        chart.unload({
            ids: [$(this).text()]
        });
    });
    setTimeout(function(){
        if (timeout && timeout == true) {
            getData(true);
        }
    },(1000*refreshSeconds));
}

function formatData(data, fieldName) {
    $.each(data, function(i, item){
        data[i][fieldName] = data[i].value;
    });
    return data;
}

function getFieldOptions(fields) {
    var fieldOptions = [];
    $.each(fields, function(i, item){
        var fieldOption = {};
        fieldOption.value = fields[i].id;
        fieldOption.label = fields[i].name;
        fieldOptions.push(fieldOption);
    });
    return fieldOptions;
}