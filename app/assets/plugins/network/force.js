$(document).ready(function () {

	var configs = [
		{node: "G", parent: null, role: "gateway"},
		{node: "1", parent: "G", role: "router"},
		{node: "2", parent: "G", role: "node"},
		{node: "3", parent: "1", role: "node"},
		{node: "4", parent: "1", role: "node"},
		{node: "5", parent: "1", role: "node"}
	];

	var nodes = {};
	var links = [];
	var width = 800;
	var height = 500;
	var force, svg; 

	// Add unique nodes to kvp
	configs.forEach(function(config) {
		nodes[config.node] = { name: config.node, role: config.role };
	});

	// Assign nodes into links
	configs.forEach(function(config) {
		if (config.parent != null) {
			links.push({ source: nodes[config.node], target: nodes[config.parent] });
		}
	});

	console.log(links);
	console.log(nodes);

	force = d3.layout.force()
		.size([$("#network-diagram").width(), $("#network-diagram").height()])
		.nodes(d3.values(nodes))
		.links(links)
		.on("tick", tick)
		.linkDistance(100)
		.charge(function (d) {
			if (d.role == "gateway") {
				return -1000;
			} else {
				return -350;
			}
		})
		.start();

	svg = d3.select("#network-diagram").append("svg")
		.attr("style", "width:100%;height:100%;");

	links = svg.selectAll(".link")
		.data(links)
		.enter().append("line")
		.attr("class", "link");

	nodes = svg.selectAll(".node")
		.data(d3.values(nodes))
		.enter().append("g")
		.call(force.drag);

	nodes.append("circle")
		.attr("class", function(d) { return d.role; })
		.attr("r", 20)

	nodes.append("text")
			.attr("dx", "-.3em")
			.attr("dy", ".25em")
			.text(function(d) { return d.name; });

	function tick() {
		links.attr("x1", function(d) { return d.source.x; })
			.attr("y1", function(d) { return d.source.y; })
			.attr("x2", function(d) { return d.target.x; })
			.attr("y2", function(d) { return d.target.y; });

		nodes.attr("transform", function(d) { 
			return "translate(" + d.x + "," + d.y + ")"; 
		});

		// Make this more efficient later
		force.size([$("#network-diagram").width(), $("#network-diagram").height()]);
	}

});