/**
 * Created by Matthew on 5/31/2017.
 */
$(document).ready(function() {

    toggleErrorDisplay();

    $(".window").each(function() {
        $(this).find(".toggle-size").click(function() {
            toggleWindowSize($(this).closest(".window"));
        });
        $(this).find(".remove").click(function() {
            deleteWindow($(this).closest(".window"));
        });
    });

    function toggleWindowSize(windowEl) {
        $.ajax({
            url: "/windows/" + windowEl.data("id") + "/resize",
            type: "PUT",
            success: function(result) {
                if (result.error) {
                    addError(result.error);
                } else {
                    windowEl.closest(".window-container").attr("class", "col-md-" + result.width + " window-container");
                    windowEl.attr("style", "height:" + result.height + "px;");
                    content = windowEl.find(".content");
                    content.attr("src", content.attr("src")); // Refresh
                }
            },
            fail: function(response,status) {
                addError("Could not resize window (request issue)");
                return false;
            }
        });
    }

    function deleteWindow(windowEl) {
        container = windowEl.closest(".window-container");
        $.ajax({
            url: "/windows/" + windowEl.data("id"),
            type: "DELETE",
            success: function(result) {
                if (result.error) {
                    addError(result.error);
                } else {
                    container.remove();
                }
            },
            fail: function(response,status) {
                addError("Could not delete window (request issue)");
                return false;
            }
        });
    }
});