/**
 * Created by Matthew on 5/31/2017.
 */
$(document).ready(function(){
    $(".sortable-tablebody").sortable({
        items: "tr.sortable-node",
        connectWith: ".node-drop-zone, .free-node-drop-list",
        handle: ".sort-selector",
        cancel: ".text",
        helper: fixHelper,
        receive: changedSortableTable
    });

    var trs = $(".free-node-drop-list").find('tr:visible');
    if (trs == null || trs.size() == 0) {
        $(".empty-node-list").show();
    } else {
        $(".empty-node-list").hide();
    }
});

var fixHelper = function(event, ui) {
    ui.children().each(function() {
        $(this).width($(this).width());
    });
    return ui;
};

var changedSortableTable = function (event, ui) {
    var trs = $(".free-node-drop-list").find('tr:visible').not('.empty-node-list');
    if (trs == null || trs.size() == 0) {
        $(".empty-node-list").show();
    } else {
        $(".empty-node-list").hide();
    }

    if (ui.item.closest('tbody').hasClass("free-node-drop-list")) {
        var nodeId = ui.item.find("#id").val();
        var data = {"node_id": nodeId};
        $.ajax({
            url: "devices/removelinknodetogateway",
            method: "PUT",
            data: data,
        }).done(function (response) {
            displaySuccessMsg(response.success);
        }).fail(function (response) {
            displayErrorMsg(response.error);
            sender.sortable('cancel');
        });
    } else {
        var sender = ui.sender;
        var nodeId = ui.item.find("#id").val();
        var gatewayId = ui.item.closest('table').find('.gateway-row').find('#id').val();
        var data = {"node_id": nodeId, "gateway_id": gatewayId};
        $.ajax({
            url: "devices/addlinknodetogateway",
            method: "PUT",
            data: data,
        }).done(function (response) {
            displaySuccessMsg(response.success);
        }).fail(function (response) {
            displayErrorMsg(response.error);
            sender.sortable('cancel');
        });
    }
};