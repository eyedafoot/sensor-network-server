// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery2
//= require jquery-ui
//= require jquery_ujs
//= require js.cookie
//= require bootstrap
//= require bootstrap-multiselect
// bootstrap-sprockets conflicts with bootstrap-multiselect
// require bootstrap-sprockets

// require_tree .

$(document).ready(function(){
    $('.dropdown-toggle').dropdown();
});

function setResourceVars(resourceName, id, vars) {
	resourceVars = getResourcesVars(resourceName);
	resourceVars["id" + id] = vars;
	createCookie("_SensorNetwork_" + resourceName, JSON.stringify(resourceVars), 30);
}

function getResourceVars(resourceName, id) {
	resourcesVars = JSON.parse(readCookie("_SensorNetwork_" + resourceName)) || { };
	return resourcesVars["id" + id] || { };
}

function getResourcesVars(resourceName) {
	resourcesVars = JSON.parse(readCookie("_SensorNetwork_" + resourceName)) || { };
	return resourcesVars || { };
}

function createCookie(name,value,days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires = "; expires="+date.toGMTString();
    }
    else var expires = "";
    document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name,"",-1);
}

function addError(message) {
	$("#current-errors").find("ul").append("<li>" + message + "</li>");
	toggleErrorDisplay();
}

function isError() {
	if ($("#current-errors").find("li").length > 0) {
		return true;
	} else {
		return false;
	}
}

function toggleErrorDisplay() {
	if ($("#current-errors").find("li").length == 0) {
		$("#current-errors").attr("style", "display:none;");
	} else {
		$("#current-errors").removeAttr("style");
	}
}