$(document).ready(function() {
	$("#device-type-node").click();
});

function setTable(deviceType) {
    
	$("table").each(function() {
		if ($(this).hasClass(deviceType)) {
			$(this).show();
		} else {
			$(this).hide();
		}
	});
}