//= require codemirror
//= require codemirror/modes/xml
//= require codemirror/modes/htmlmixed
//= require codemirror/modes/javascript
//= require codemirror/modes/ruby
//= require codemirror/modes/haml

$(document).ready(function() {
	CodeMirror.fromTextArea($("#plugin_css")[0], {
		mode: "css",
		lineNumbers: true,
		lineWrapping: true
	});

	CodeMirror.fromTextArea($("#plugin_javascript")[0], {
		mode: "javascript",
		lineNumbers: true,
		lineWrapping: true
	});

	CodeMirror.fromTextArea($("#plugin_html")[0], {
		mode: "htmlmixed",
		lineNumbers: true,
		lineWrapping: true
	});
});