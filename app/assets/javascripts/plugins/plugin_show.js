//= require d3.js
//= require c3

function getNodes(windowId) {
    return Cookies.getJSON("nodes_window_id_"+windowId);
}

function getDevice(windowId) {
    return Cookies.getJSON("device_window_id_"+windowId);
    /*var params = "window_id=" + windowId;

    return $.ajax({
            url:"/plugins/gateway?" + params,
            method: "GET",
            cache: false,
            async: false
        }).done(function(data){
            return data;
        }).responseJSON;*/
}

function getNodeFieldData(nodeId, fieldId, numSamples, data, fromDate, toDate, async) {
    if (async === undefined) {
        async = false;
    }

    var params = "field_id=" + fieldId + "&node_id=" + nodeId + "&num_samples=" + numSamples;
    if (data != null && data.length) {
        let maxId = data[data.length-1].id;
        for (let x = 0; x < data.length; x++) {
            if (data[x].id > maxId) {
                maxId = data[x].id;
            }
        }
        params = params + "&last_id=" + maxId;
    }
    if (fromDate != null) {
        params = params + "&from_date="+fromDate;
    }
    if (toDate != null) {
        params = params + "&to_date="+toDate;
    }
    return $.ajax({
        url:"/plugins/data?" + params,
        method: "GET",
        cache: false,
        async: async
    }).done(function(data){
        return data;
    }).responseJSON;
}

function getGatewayCommands(gatewayId, async) {
    if (async === undefined) {
        async = false;
    }

    var params = "gateway_id=" + gatewayId;

    return $.ajax({
        url:"/plugins/commands?" + params,
        method: "GET",
        cache: false,
        async: async
    }).done(function(data){
        return data;
    }).responseJSON;
}

function getFields(nodeId, async) {
    if (async === undefined) {
        async = false;
    }

    var params = "node_id=" + nodeId;

    return $.ajax({
        url:"/plugins/fields?" + params,
        method: "GET",
        cache: false,
        async: async
    }).done(function(data){
        return data;
    }).responseJSON;
}

function createGatewayCommand(gatewayId, command, async) {
    if (async === undefined) {
        async = false;
    }

    if (Command.isPrototypeOf(command)) {
        var params = "gateway_id=" + gatewayId;

        var postStr = JSON.stringify(command);
        var post = JSON.parse(postStr);

        return $.ajax({
            url: "/plugins/command?" + params,
            method: "POST",
            data: post,
            dataType: 'json',
            cache: false,
            async: async
        }).done(function (data) {
            return data;
        }).responseJSON;
    } else {
        return {"error": 'Command input format doesn\'t match desired format'};
    }
}

var Parameter = {
    init: function(name, value) {
        this.name = name;
        this.value = value;
        return this;
    }
};

var Command = {
    init: function (name, priority, parameters) {
        this.name = name;
        this.priority = priority;
        this.setParameters(parameters);
        return this;
    },
    setParameters: function(parameters){
        if (parameters == null){
            this.parameters = [];
            return this;
        }
        if (!Array.isArray(parameters)) {
            parameters = [parameters];
        }
        if (parameters.every(this.isParameter)) {
            this.parameters = $.extend(true, [], parameters);
        }
        return this;
    },
    setParameter: function(name, value){
        var param = Object.create(Parameter);
        this.parameters = [param.init(name,value)];
        return this;
    },
    addParameters: function(parameters) {
        if (!Array.isArray(parameters)) {
            parameters = [parameters];
        }
        if (parameters.every(this.isParameter)) {
            length = parameters.length;
            for (var i = 0; i < length; i++){
                this.parameters.push(parameters[i]);
            }
        }
        return this;
    },
    addParameter: function(name, value){
        var param = Object.create(Parameter);
        this.parameters.push(param.init(name,value));
        return this;
    },
    isParameter: function (parameter, index, array) {
        return Parameter.isPrototypeOf(parameter)
    }
};
/*
* OLD FUNCTIONS
* These commands are deprecated in light of more user friend and understandable functions and objects
*
* */
function setupWindowVariables(winVars, requiredVarToPlaceholder, optionalVarToDefault) {
	requiredVarToPlaceholder = requiredVarToPlaceholder || {};
	optionalVarToDefault = optionalVarToDefault || {};
	for (var requiredVar in requiredVarToPlaceholder) {
		var varType = typeof(requiredVarToPlaceholder[requiredVar]);
		var varVal = $("#var-"+varType+"-template").clone();

		if (varVal.length) {
			// Add the variable input to the template
			winVar = $("#window-var-template").clone();
			winVar.removeAttr("id");
			winVar.append(varVal);
			varVal.removeAttr("id");
			varVal.show();
			winVar.show();

			$("#window-vars").append("<br/>");
			$("#window-vars").append(winVar);

			if (winVars[requiredVar]) {
				varVal.find("input").val(winVars[requiredVar]);
			} else {
				varVal.find("input").val(requiredVarToPlaceholder[requiredVar]);
				addError("Required variable not specified: " + requiredVar);
			}
			winVar.find("label").text(requiredVar);

		} else {
			addError("Invalid variable type for: " + requiredVar); 
		}
	}

	for (var optionalVar in optionalVarToDefault) {
		var varType = typeof(optionalVarToDefault[optionalVar]);
		var varVal = $("#var-"+varType+"-template").clone();

		if (varVal.length) {
			// Add the variable input to the template
			winVar = $("#window-var-template").clone();
			winVar.removeAttr("id");
			winVar.append(varVal);
			varVal.removeAttr("id");
			varVal.show();
			winVar.show();

			$("#window-vars").append("<br/>");
			$("#window-vars").append(winVar);
			varVal.find("input").val(winVars[optionalVar] || optionalVarToDefault[optionalVar]);
			winVars[optionalVar] = winVars[optionalVar] || optionalVarToDefault[optionalVar];
			winVar.find("label").text(optionalVar);

		} else {
			addError("Invalid variable type for: " + optionalVar); 
		}
	}
	if (isError()) {
		$("#user-content-container").hide();
		return false;
	} else {
		return true;
	}
}

$(document).ready(function () {
	$("#toggle-window-vars").click(function () {
		$("#window-content-container").toggle();
		$("#window-vars-container").toggle();
	});

	// Sets the windows variables into a cookie, then reload
	$("#window-vars-update").click(function () {
		var winVars = { };
		var varEl = $("#window-vars").find(".variable");
		varEl.each(function () {
			winVars[$(this).find("label").text()] = $(this).find("input").val();
		});
		setResourceVars("window", $("body").data("id"), winVars);
		location.reload();
	});
});
