/**
 * Created by Matthew on 1/17/2018.
 */
$(function() {
    $('._paramCollapse').on('click', function(){
        var $_this = $(this);
        setTimeout(function(){
            if ($_this.hasClass('collapsed')) {
                $_this.find("span").addClass("glyphicon-chevron-down").removeClass("glyphicon-chevron-up");
            } else {
                $_this.find("span").removeClass("glyphicon-chevron-down").addClass("glyphicon-chevron-up");
            }
        }, 100);
    });

    $('._addCommand').unbind().on('click', function() {
        var $_this = $(this);
        var commandForm = $_this.closest('form').serializeArray();
        var command = getCommand(commandForm);

        saveCommand(command, $_this);
    });

    $('._updateCommand').unbind().on('click', function() {
        var $_this = $(this);
        var commandForm = $_this.closest('form').serializeArray();
        var command = getCommand(commandForm);

        updateCommand(command, $_this);
    });

    $('._removeCommand').unbind().on('click', function() {
        var $_this = $(this);
        var commandForm = $_this.closest('form').serializeArray();
        var command = getCommand(commandForm);

        deleteCommand(command, $_this);
    });

    $('._addParam').unbind().on('click', function() {
        var $_this = $(this);
        var paramForm = $_this.closest('form').serializeArray();
        var param = getParam(paramForm);

        saveParam(param, $_this);
    });

    $('._updateParam').unbind().on('click', function() {
        var $_this = $(this);
        var paramInputs = $_this.closest('tr').find('input, select');
        var param = getParam(null, paramInputs);

        updateParam(param, $_this);
    });

    $('._removeParam').unbind().on('click', function() {
        var $_this = $(this);
        var paramInputs = $_this.closest('tr').find('input, select');
        var param = getParam(null, paramInputs);

        deleteParam(param, $_this);
    });
});

function saveCommand(command, $_button) {
    $.ajax({
        url: "/commands?gateway_id=" + command.gateway_id,
        type: "POST",
        data: command,
        success: function(command) {
            displaySuccessMsg("Successfully saved command");
            addCommand($_button)
        },
        fail: function(response,status) {
            displayErrorMsg("Failed to save command");
        }
    });
}

function updateCommand(command, $_button) {
    $.ajax({
        url: "/commands/" + command.id + "?gateway_id=" + command.gateway_id,
        type: "PUT",
        data: command,
        success: function(command) {
            displaySuccessMsg("Successfully updated command");
        },
        fail: function(response,status) {
            displayErrorMsg("Failed to update command");
        }
    });
}

function deleteCommand(command, $_button) {
    $.ajax({
        url: "/commands/" + command.id + "?gateway_id=" + command.gateway_id,
        type: "DELETE",
        success: function(command) {
            displaySuccessMsg("Successfully removed command");
            removeCommand($_button, command)
        },
        fail: function(response,status) {
            displayErrorMsg("Failed to remove command");
        }
    });
}

function saveParam(param, $_button) {
    $.ajax({
        url: "/commands/" + param.command_id + "/parameter?gateway_id=" + param.gateway_id,
        type: "POST",
        data: param,
        success: function(param) {
            displaySuccessMsg("Successfully saved parameter");
            addParam($_button, param)
        },
        fail: function(response,status) {
            displayErrorMsg("Failed to save parameter");
        }
    });
}

function updateParam(param, $_button) {
    $.ajax({
        url: "/commands/" + param.command_id  + "/parameter/" + param.id + "?gateway_id=" + param.gateway_id,
        type: "PUT",
        data: param,
        success: function(command) {
            displaySuccessMsg("Successfully updated parameter");
        },
        fail: function(response,status) {
            displayErrorMsg("Failed to update parameter");
        }
    });
}

function deleteParam(param, $_button) {
    $.ajax({
        url: "/commands/" + param.command_id  + "/parameter/" + param.id + "?gateway_id=" + param.gateway_id,
        type: "DELETE",
        data: param,
        success: function(command) {
            displaySuccessMsg("Successfully removed parameter");
            removeParam($_button)
        },
        fail: function(response,status) {
            displayErrorMsg("Failed to remove parameter");
        }
    });
}

function getCommand(commandForm) {
    var command = {};
    $.each(commandForm, function(key, value) {
        switch(value.name) {
            case'command[gateway_id]':
                command.gateway_id = value.value;
                break;
            case'command[device_id]':
                command.device_id = value.value;
                break;
            case'command[id]':
                command.id = value.value;
                break;
            case'command[active]':
                command.active = value.value;
                break;
            case'command[priority]':
                command.priority = value.value;
                break;
            case'command[name]':
                command.name = value.value;
                break;
        }
    });
    return command;
}

function getParam(paramForm, paramInputs) {
    var param = {};

    if (paramForm) {
        $.each(paramForm, function (key, value) {
            switch (value.name) {
                case'param[gateway_id]':
                    param.gateway_id = value.value;
                    break;
                case'param[value]':
                    param.value = value.value;
                    break;
                case'param[id]':
                    param.id = value.value;
                    break;
                case'param[name]':
                    param.name = value.value;
                    break;
                case'param[command_id]':
                    param.command_id = value.value;
                    break;
            }
        });
    } else if (paramInputs) {
        $.each(paramInputs, function (key, value) {
            switch (value.name) {
                case'param[gateway_id]':
                    param.gateway_id = value.value;
                    break;
                case'param[value]':
                    param.value = value.value;
                    break;
                case'param[id]':
                    param.id = value.value;
                    break;
                case'param[name]':
                    param.name = value.value;
                    break;
                case'param[command_id]':
                    param.command_id = value.value;
                    break;
            }
        });
    }
    return param;
}

function addCommand($_button, command){
    location.reload();
}

function removeCommand($_button){
    var form = $_button.closest('form');
    var commandId = form.find('input[name="command[id]"]').val();
    var paramDiv = form.next('div[id="command_'+commandId+'_params"]');

    form.remove();
    paramDiv.remove();
}

function addParam($_button, param){
    location.reload();
}

function removeParam($_button){
    var tr = $_button.closest('tr');

    tr.remove();
}