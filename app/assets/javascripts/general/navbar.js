/**
 * Created by Matthew on 6/3/2017.
 */

$(document).ready(function(){
    setInterval(function() {
        setDeviceApprovalCounter();
    }, 1000 * 60 * 2);

    setDeviceApprovalCounter();
});

function setDeviceApprovalCounter() {
    $.ajax({
        url:"/devices/queuednodecount",
        method: "GET",
        cache: false
    }).done(function(data){
        $('#deviceBadge').text(data);
    })
}