$(document).ready(function() {
	$("#dock-link").click(function() {
		$(this).parent().addClass("active");
		$("#commands-link").parent().removeClass("active");
        $("#nodes-link").parent().removeClass("active");
        $("#gateway-dock").show();
		$("#gateway-commands").hide();
        $("#gateway-nodes").hide();
        window.location.hash = 'dock';
	});

	$("#commands-link").click(function() {
		$(this).parent().addClass("active");
		$("#dock-link").parent().removeClass("active");
        $("#nodes-link").parent().removeClass("active");
		$("#gateway-dock").hide();
		$("#gateway-commands").show();
        $("#gateway-nodes").hide();
        window.location.hash = 'commands';
	});

    $("#nodes-link").click(function() {
        $(this).parent().addClass("active");
        $("#commands-link").parent().removeClass("active");
        $("#dock-link").parent().removeClass("active");
        $("#gateway-dock").hide();
        $("#gateway-commands").hide();
        $("#gateway-nodes").show();
        window.location.hash = 'nodes';
    });

    $(function() {
        if(window.location.hash == "#commands"){
            $("#commands-link").click();
        }else if(window.location.hash == "#nodes"){
            $("#nodes-link").click();
        } else {
            $("#dock-link").click();
        }
    })
});

