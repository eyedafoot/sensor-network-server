var totalRows = 0;
var rowsSaved = 0;

$(document).ready(function() {
	hash = window.location.hash.substring(1);

	if (hash == "settings") {
		showSettings();
	}

	$("#dock-link").click(function() {
		showDock();
	});

	$("#settings-link").click(function() {
		showSettings();
	});

	$("#add-field").click(function() {
		form = $(this).closest("form");
		$.ajax({
			url: form.attr("action"),
			type: "POST",
			data: form.serialize() ,
			success: function(field) {
				if (field.error) {
                    displayErrorMsg(field.error);
				} else {
                    displaySuccessMsg("Succesfully created field");
                    form.closest('div.buttons').prev('div.field-list').children('form').last().after(field);
                    addDeleteButton();
				}
			},
			fail: function(response,status) {
                displayErrorMsg("Could not create field (request issue)");
			}
		});
	});

    addDeleteButton();

	$("#update-fields").click(function() {
        totalRows = $(".field-form").length;
        rowsSaved = 0;
		$(".field-form").each(function() {
			$.ajax({
				url: $(this).attr("action"),
				type: "PUT",
				data: $(this).serialize() ,
				success: function(data) {
					if (data.error) {
                        displayErrorMsg(data.error);
					} else {
                        displaySuccessMsg(data.success);
                    }
                    checkReadyToReloadPage();
				},
				fail: function(response,status) {
                    displayErrorMsg("Could not update field (request issue)");
				}
			});
		});
	});

});

function checkReadyToReloadPage() {
    rowsSaved += 1;
    if (rowsSaved == totalRows) {
        location.reload();
    }
}

function addDeleteButton(){
    $(".remove-field").each(function() {
        $(this).click(function() {
            form  = $(this).closest(".field-form");
            $.ajax({
                url: form.attr("action"),
                type: "DELETE",
                data: form.serialize() ,
                success: function(response) {
                    if (response.error) {
                        displayErrorMsg(response.error);
                    } else {
                        displaySuccessMsg("Successfully deleted field");
                        form.remove();
                    }
                },
                fail: function(response,status) {
                    displayErrorMsg("Could not remove field (request issue)");
                }
            });
        });
    });
}

function showSettings() {
	$("#settings-link").parent().addClass("active");
	$("#dock-link").parent().removeClass("active");	
	$("#node-dock").hide();
	$("#node-settings").show();
}

function showDock() {
	$("#dock-link").parent().addClass("active");
	$("#settings-link").parent().removeClass("active");
	$("#node-settings").hide();
	$("#node-dock").show();
}

function exportData(select, nodeId, fieldId) {
    if ($(select).val() == null || $(select).val() == ''){
        return;
    }

    var url = '/data/export_data.' + $(select).val() + '?node_id=' + nodeId + '&field_id=' + fieldId;
    window.location = url;
    return;
}