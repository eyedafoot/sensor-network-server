/**
 * Created by Matthew on 6/2/2017.
 */
function editItem(editBtn) {
    var $editBtn = $(editBtn);
    var label = $editBtn.closest('tr').find('label[id="description"]');
    var input = $editBtn.closest('tr').find('input[id="description"]');

    label.hide();
    input.show();

    var saveBtn = $editBtn.closest('td').find('button[id="saveItem"]');

    saveBtn.show();
    $editBtn.hide();

    var cancelBtn = $editBtn.closest('td').next('td').find('button[id="cancelItem"]');
    var deleteBtn = $editBtn.closest('td').next('td').find('button[id="deleteItem"]');

    cancelBtn.show();
    deleteBtn.hide();
}

function saveItem(saveBtn) {
    var $saveBtn = $(saveBtn);
    var id = $saveBtn.closest('tr').find('input[id="setting_id"]').val();
    var itemCode = $saveBtn.closest('tr').find('input[id="item_code"]').val();
    var description = $saveBtn.closest('tr').find('input[id="description"]').val();

    $.ajax({
        url:"/settings/" + id +"?item_code="+itemCode+"&item_description=" + description,
        method: "PUT",
        cache: false,
        async: false
    }).success(function(data){
        displaySuccessMsg(data.success);

        var label = $saveBtn.closest('tr').find('label[id="description"]');
        var input = $saveBtn.closest('tr').find('input[id="description"]');
        var createdAtLbl = $saveBtn.closest('tr').find('label[id="createdAt"]');
        var updatedAtLbl = $saveBtn.closest('tr').find('label[id="updatedAt"]');
        var inputId = $saveBtn.closest('tr').find('input[id="setting_id"]');

        label.text(data.setting.item_description);
        createdAtLbl.text(data.setting.created_at);
        updatedAtLbl.text(data.setting.updated_at);
        inputId.val(data.setting.id);

        label.show();
        input.hide();

        var editBtn = $saveBtn.closest('td').find('button[id="editItem"]');

        editBtn.show();
        $saveBtn.hide();

        var deleteBtn = $saveBtn.closest('td').next('td').find('button[id="deleteItem"]');
        var cancelBtn = $saveBtn.closest('td').next('td').find('button[id="cancelItem"]');

        deleteBtn.show();
        cancelBtn.hide();
    }).error(function(data){
        displayErrorMsg(data.error);

        var label = $saveBtn.closest('tr').find('label[id="description"]');
        var input = $saveBtn.closest('tr').find('input[id="description"]');

        //input.val(label.text());
    });
}

function deleteItem(deleteBtn) {
    var $deleteBtn = $(deleteBtn);
    var id = $deleteBtn.closest('tr').find('input[id="setting_id"]').val();

    $.ajax({
        url:"/settings/" + id,
        method: "DELETE",
        cache: false,
        async: false
    }).success(function(data){
        displaySuccessMsg(data.success);

        var tr = $deleteBtn.closest('tr');
        tr.remove();
    }).error(function(data){
        displayErrorMsg(data.error);
    });
}

function cancelItem(cancelBtn) {
    //TODO: Maybe do ajax call to make sure no one else edited this setting
    var $createdBtn = $(cancelBtn);
    var createdAt = $createdBtn.closest('tr').find('label[id="createdAt"]');
    var updatedAt = $createdBtn.closest('tr').find('label[id="updatedAt"]');

    if (createdAt.text() == '' && updatedAt.text() == '')
    {
        $createdBtn.closest('tr').remove();
        return;
    }

    var label = $createdBtn.closest('tr').find('label[id="description"]');
    var input = $createdBtn.closest('tr').find('input[id="description"]');

    label.show();
    input.hide();

    var editBtn = $createdBtn.closest('tr').find('button[id="editItem"]');
    var saveBtn = $createdBtn.closest('tr').find('button[id="saveItem"]');

    editBtn.show();
    saveBtn.hide();

    var deleteBtn = $createdBtn.closest('td').find('button[id="deleteItem"]');

    deleteBtn.show();
    $createdBtn.hide();
}

function newItem(itemCode){
    var $table = $('#itemTable');

    var $newTr = $(document.createElement('tr'));
    var descriptionTd = '<td><label id="description" style="display:none;"></label>' +
        '<input id="description" class="form-control"/>' +
        '<input id="setting_id" value="0" type="hidden"/>' +
        '<input id="item_code" value="' + itemCode + '" type="hidden"/></td>';
    var createdAtTd = '<td><label id="createdAt"></label></td>';
    var updatedAtTd = '<td><label id="updatedAt"></label></td>';
    var editSaveTd = '<td><button id="editItem" class="btn btn-primary" onclick="editItem(this);" style="display:none;">Edit</button>' +
        '<button id="saveItem" class="btn btn-primary" onclick="saveItem(this);">Save</button></td>';
    var deleteCancelTd = '<td><button id="deleteItem" class="btn btn-primary" onclick="deleteItem(this);" style="display:none;">Delete</button>' +
        '<button id="cancelItem" class="btn btn-primary" onclick="cancelItem(this);">Cancel</button></td>';

    $newTr.append(descriptionTd, createdAtTd, updatedAtTd, editSaveTd, deleteCancelTd);

    $table.append($newTr);
}