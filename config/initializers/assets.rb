# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
# application.js, application.css.scss, and all non-JS/CSS in app/assets folder are already added.
# Rails.application.config.assets.precompile += %w( search.js )

Rails.application.config.assets.precompile += %w( datetimepicker.css )
Rails.application.config.assets.precompile += %w( datetimepicker.js )

%w( gateways nodes ).each do |controller|
  Rails.application.config.assets.precompile += ["#{controller}.css"]
end

%w( window_show command ).each do |action|
  Rails.application.config.assets.precompile += ["#{action}.css"]
end

%w( networks_index node_show gateway_show command window_show ).each do |action|
  Rails.application.config.assets.precompile += ["#{action}.js"]
end

#----general sub directory----#
%w( navbar  ).each do |action|
  Rails.application.config.assets.precompile += ["general/#{action}.js"]
end

#----devices sub directory----#
%w( devices_list devices ).each do |action|
  Rails.application.config.assets.precompile += ["devices/#{action}.css"]
end

%w( devices_list device_show ).each do |action|
  Rails.application.config.assets.precompile += ["devices/#{action}.js"]
end

#----plugins sub directory----#
%w( plugin_edit plugin_show ).each do |action|
  Rails.application.config.assets.precompile += ["plugins/#{action}.css"]
end

%w( plugin_edit plugin_show ).each do |action|
  Rails.application.config.assets.precompile += ["plugins/#{action}.js"]
end

#----settings sub directory----#
%w( picklist ).each do |action|
  Rails.application.config.assets.precompile += ["settings/#{action}.css"]
end

%w( picklist ).each do |action|
  Rails.application.config.assets.precompile += ["settings/#{action}.js"]
end