Rails.application.routes.draw do

	root 'network#index'

  #User Login Web Routes
	get 'login', to: 'sessions#new'
	get 'logout', to: 'sessions#destroy'
	post 'sessions', to: 'sessions#create'

  #User Web Routes
	get 'users/list', to: 'users#user_list'
	resources :users, except: [:index, :show]

	#Routes for plugin ajax calls
	get 'plugins/gateway', to: 'plugins#get_gateway'
	get 'plugins/nodes', to: 'plugins#get_nodes'
	get 'plugins/fields', to: 'plugins#get_fields'
	get 'plugins/data', to: 'plugins#get_field_data'
	get 'plugins/commands', to: 'plugins#get_gateway_commands'
	post 'plugins/command', to: 'plugins#create_gateway_command'

  #Plugin Web Routes
  get 'plugins/my_plugins', to: 'plugins#my_plugins'
  get 'plugins/tutorial', to: 'plugins#tutorial'
  resources :plugins, except: [:new]

  #Network Web Routes
	post 'network/connect', to: 'network#connect'
	post 'network/update', to: 'network#update'
	get 'network', to: 'network#index'

  #Node Web Routes
	resources :nodes, only: [:show]
	get 'update', to: 'nodes#update'

  #Gateway Web Routes
	resources :gateways, only: [:show]

  #Access Rights Web Routes
	resources :access_rights, only: [:index, :create, :destroy]

  #Devices Routes
  put 'devices/addlinknodetogateway',to: 'devices#add_link_node_to_gateway'
  put 'devices/removelinknodetogateway',to: 'devices#remove_link_node_to_gateway'
	get 'devices/queuednodecount', to: 'devices#number_of_queued_nodes'
	put 'devices/acceptedqueuednode', to: 'devices#accept_queued_node'
	put 'devices/rejectqueuednode', to: 'devices#reject_queued_node'
	resources :devices

  #Windows Web Routes
	resources :windows, only: [:show, :create, :destroy] do
		member do 
			put :backward
			put :forward
			put :resize
		end
	end

  #Command Web Routes
	resources :commands, only: [:show, :create, :destroy, :update]do
		member do 
			get :execute
      get :next
		end
  end
  post 'commands/:command_id/parameter', to: 'commands#create_param'
	put 'commands/:command_id/parameter/:param_id', to: 'commands#update_param'
	delete 'commands/:command_id/parameter/:param_id', to: 'commands#destroy_param'
	#get 'commands/next', to: 'commands#next'

  #Field Web Routes
	resources :fields, only: [:create, :update, :destroy]

  #Data Web Routes
	resources :data, only: [:index, :create]
  get 'data/export_data', to: 'data#export_data'

  #Settings Web Routes
  get 'settings/nodetypes', to: 'settings#picklist'
	get 'settings/commandtypes', to: 'settings#picklist'
  resources :settings, only: [:index, :update, :destroy]

  ### MOBILE API ROUTES ###
  namespace :mobileapi do
    #Data
		put 'data', to: 'data#create', defaults: {format: :json}
		post 'data', to: 'data#create_mult', defaults: {format: :json}
		post 'datafile', to: 'data#create_via_file', defaults: {format: :json}

    #Commands
    get 'command', to: 'command#show', defaults: {format: :json}
    put 'command', to: 'command#next', defaults: {format: :json}

    #Devices
    put 'devices/approvalrequest', to: 'device_approval_queue#device_approval_request', defaults: {format: :json}
    post 'devices/approvalrequest', to: 'device_approval_queue#device_approval_request', defaults: {format: :json}
    get 'devices', to: 'device_approval_queue#registered_devices', defaults: {format: :json}
  end
end
